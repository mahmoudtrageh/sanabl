@extends('admin.layouts.Master-Layout')

@section('title')

    الرئيسيه

@stop

@section('content')

    <div class="sa-content-wrapper">

        <!-- END .sa-page-breadcrumb -->
        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>> {{trans('admin.index')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>
                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <!-- NEW WIDGET START -->
                            <article class="col-12">
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                     data-widget-editbutton="false">
                                    <header>
                                        <div class="">
													<span class="widget-icon"> <i class="fa fa-lg fa-fw fa-user"></i>
													</span>
                                        </div>
                                    </header>
                                    <!-- END RIBBON -->
                                    <div id="content" style="margin-top:2rem;">



                                        <!-- row -->

                                        <div class="row">

                                            <div class="col-sm-12">


                                                <div class="well well-sm">

                                                    <div class="row">

                                                        <div class="col-sm-12 col-md-12 col-lg-12">
                                                            <div
                                                                    class="well well-light well-sm no-margin no-padding">

                                                                <div class="row">
                                                                    <div class="col-sm-12">

                                                                        <div class="row">
                                                                            <div class="col-sm-5" style="box-shadow: -2px 1px 12px 0px;		margin-left: 23px;margin-bottom:1rem;height: 10rem;			margin-right: 2rem;
																						margin-top: 1rem;">
                                                                                <p style="font-weight:bold;font-size:23px;">No. Of Visitor
                                                                                    <code> 50</code> <span
                                                                                            class="text-orange pull-right"></span>
                                                                                </p>
                                                                                <div class="progress progress-sm ">

                                                                                    <div class="progress-bar bg-red-light progress-bar-striped progress-bar-animated"
                                                                                         style="width: 45%"></div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-sm-5" style="box-shadow: -2px 1px 12px 0px;		margin-left: 23px;margin-bottom:1rem;height: 10rem;			margin-right: 2rem;
																					margin-top: 1rem;">
                                                                                <p style="font-weight:bold;font-size:23px;">No. Of Visitor
                                                                                    <code> 50</code> <span
                                                                                            class="text-orange pull-right"></span>
                                                                                </p>
                                                                                <div class="progress progress-sm ">

                                                                                    <div class="progress-bar bg-red-light progress-bar-striped progress-bar-animated"
                                                                                         style="width: 45%"></div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-sm-5" style="box-shadow: -2px 1px 12px 0px;		margin-left: 23px;margin-bottom:1rem;height: 10rem;			margin-right: 2rem;
																				margin-top: 1rem;">
                                                                                <p style="font-weight:bold;font-size:23px;">No. Of Visitor
                                                                                    <code> 50</code> <span
                                                                                            class="text-orange pull-right"></span>
                                                                                </p>
                                                                                <div class="progress progress-sm ">

                                                                                    <div class="progress-bar bg-red-light progress-bar-striped progress-bar-animated"
                                                                                         style="width: 45%"></div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-sm-5" style="box-shadow: -2px 1px 12px 0px;		margin-left: 23px;margin-bottom:1rem;height: 10rem;			margin-right: 2rem;
																			margin-top: 1rem;">
                                                                                <p style="font-weight:bold;font-size:23px;">No. Of Visitor
                                                                                    <code> 50</code> <span
                                                                                            class="text-orange pull-right"></span>
                                                                                </p>
                                                                                <div class="progress progress-sm ">

                                                                                    <div class="progress-bar bg-red-light progress-bar-striped progress-bar-animated"
                                                                                         style="width: 45%"></div>
                                                                                </div>

                                                                            </div>
                                                                            <div class="col-sm-5" style="box-shadow: -2px 1px 12px 0px;		margin-left: 23px;margin-bottom:1rem;height: 10rem;			margin-right: 2rem;
																		margin-top: 1rem;">
                                                                                <p style="font-weight:bold;font-size:23px;">No. Of Visitor
                                                                                    <code> 50</code> <span
                                                                                            class="text-orange pull-right"></span>
                                                                                </p>
                                                                                <div class="progress progress-sm ">

                                                                                    <div class="progress-bar bg-red-light progress-bar-striped progress-bar-animated"
                                                                                         style="width: 45%"></div>
                                                                                </div>

                                                                            </div>


                                                                        </div>

                                                                    </div>

                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>


                                            </div>

                                        </div>

                                        <!-- end row -->

                                    </div>
                                    <!-- end widget content -->
                                </div>
                                <!-- end widget -->

                            </article>
                            <!-- WIDGET END -->

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->

                </div>
            </div>
        </div>


        <!-- BEGIN .sa-page-footer -->
        <footer class="sa-page-footer">
            <div class="d-flex align-items-center w-100 h-100">
                <div class="footer-left">
                    SpectraApp &copy;
                    2019
                </div>

            </div>

        </footer>
        <!-- END .sa-page-footer -->


    </div>
@stop