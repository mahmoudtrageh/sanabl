<!DOCTYPE html>

<html lang="en" class="smart-style-0">
<head>
    <title>login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,500,700">
    <link rel="shortcut icon" href="{{asset('backEnd/assets/img/favicon/favicon.ico')}}  " type="image/x-icon">
    <link rel="icon" href="{{asset('backEnd/assets/img/favicon/favicon.ico')}}  " type="image/x-icon">
    <link href="{{ asset('css/admin.css')}}" media="all" rel="stylesheet" type="text/css"/>

</head>
<body class=" publicHeader-active animated fadeInDown smart-style-0">

<!-- BEGIN .sa-wrapper -->
<div class="sa-wrapper">

    <div class="sa-page-body">

        <style>
            #ajaxSpinnerContainer {
                height: 11px;
                position: absolute;
                top: 0;
                left: 0;
                z-index: 3;
                width: 100vw;
                height: 100vh;
                background-color: rgba(0, 0, 0, 0.5);
                text-align: center;
                display: none;
            }

            #ajaxSpinnerImage {
                margin-left: 20%;
                margin: auto;
                margin-top: 20%;
                color: #fff;
            }
        </style>

        <div id="ajaxSpinnerContainer">
            <img src="{{asset('backEnd/ajax-loader.gif')}}" id="ajaxSpinnerImage" title="Loading...">
        </div>
        <!-- BEGIN .sa-content-wrapper -->
        <div class="sa-content-wrapper">
            <div class="sa-content">
                <div class="main" role="main" style="margin-top: 4rem;">
                    <!-- MAIN CONTENT -->
                    <div id="content" class="container padding-top-10">
                        <div class="row">
                            <div class="col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">

                                <div class="clearfix">
                                    <div class="hero">
                                        <img src="{{asset('backEnd/assets/img/demo/iphoneview.png')}}" class="
                                             pull-right display-image" alt="" style="width:210px">
                                    </div>
                                </div>


                            </div>
                            <div class="col-sm-12 col-lg-4">
                                <div class="well no-padding">
                                    <form action="{{route('admin.auth.post.login')}}" method="post" id="login-form"
                                          class="smart-form client-form">
                                        @csrf
                                        <header>
                                            Sign In
                                        </header>

                                        <fieldset>

                                            <section>
                                                <label class="label">E-mail</label>
                                                <label class="input mb-3"> <i class="icon-append fa fa-user"></i>
                                                    <input type="text" name="email">
                                                    <b class="tooltip tooltip-top-right"><i
                                                                class="fa fa-user txt-color-teal"></i> Please enter
                                                        email address/username</b></label>
                                            </section>

                                            <section>
                                                <label class="label">Password</label>
                                                <label class="input mb-1"> <i class="icon-append fa fa-lock"></i>
                                                    <input type="password" name="password">
                                                    <b class="tooltip tooltip-top-right"><i
                                                                class="fa fa-lock txt-color-teal"></i> Enter your
                                                        password</b> </label>
                                            </section>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn sa-btn-primary">
                                                Sign in
                                            </button>
                                        </footer>
                                    </form>

                                </div>


                            </div>
                        </div>
                    </div>

                </div>

            </div>


        </div>
        <!-- END .sa-content-wrapper -->


    </div>


</div>
<script type="text/javascript" src="{{asset('backEnd/sweetalert2.all.min.js')}}"> </script>
<!-- END .sa-wrapper -->
<script type="text/javascript" src="{{asset('js/admin.js')}}"></script>

<script>
    $(function () {
        $('#menu1').metisMenu();
    });
</script>
<script>
    $.ajaxSetup({cache: false});
</script>
{{--login script--}}
<script>
    $(document).on('submit', '#login-form', function (e) {
        e.preventDefault();
        let form = $('#login-form');
        let data = form.serialize();
        let spinner = $('#ajaxSpinnerContainer');
        $.ajax({
            data: data,
            url: form.attr('action'),
            type: form.attr('method'),
            dataType: 'json',
            ajaxStart: spinner.show(),
            success: function (data) {
                if (data.status === '0') {
                    Swal.fire({
                        type: 'error',
                        title: 'حدث خطأ',
                        html: '<p style="font-weight: bold;font-size: 15px;color: red;">' + data.message + '</p>',
                        width: '40rem',
                    })
                } else if (data.status === '1') {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 2000,
                        background: 'rgb(169,3,41)'

                    });

                    Toast.fire({
                        type: 'success',
                        title: '<span style="font-size: 18px ; color: #ffffff"> تم تسجيل الدخول بنجاح </span>'
                    });
                    setTimeout(function () {
                        window.location = data.url;
                    }, 1000)
                }

            },
            complete: function () {
                spinner.hide();
            }
        });

    })
</script>

</body>
</html>