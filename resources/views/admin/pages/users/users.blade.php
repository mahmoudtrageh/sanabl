@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.users')}}

@stop

@section('content')
    <div class="sa-content-wrapper">
        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>> {{trans('admin.users')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>
                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <!-- NEW WIDGET START -->
                            <article class="col-12">
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                     data-widget-editbutton="false">
                                    <header>
                                        <div class="widget-header">
                                            <span class="widget-icon"> <i class="fa fa-lg fa-fw fa-sitemap"></i> </span>
                                        </div>
                                    </header>
                                    <!-- widget div-->
                                    <div style="text-align: center;">
                                        <a style=" margin-top: 1rem;" href="javascript:void(0);"
                                           class="btn sa-btn-primary" data-toggle="modal"
                                           data-target="#myModal-1"> {{trans('admin.add.user')}}</a>


                                        <!-- Modal  to add user-->
                                        <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"
                                                            style="font-weight:bold">{{trans('admin.add.user')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align:left;">
                                                        <form action="{{route('admin.users.add')}}" method="post">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.name')}}</label>
                                                                <input type="text" class="form-control"
                                                                       id="exampleInputText12" placeholder="{{trans('admin.name')}}"
                                                                       name="name" value="{{old('name')}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputPassword1">{{trans('admin.email')}}</label>
                                                                <input type="Email" class="form-control"
                                                                       id="exampleInputEmail12" placeholder="{{trans('admin.email')}}"
                                                                       name="email" value="{{old('email')}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputPassword1">{{trans('admin.phone')}}</label>
                                                                <input type="number" class="form-control"
                                                                       id="exampleInputEmail144" placeholder="{{trans('admin.phone')}}"
                                                                       name="phone" value="{{old('phone')}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.country')}}</label>
                                                                <select name="country" class="form-control"
                                                                        id="country_add">
                                                                    <option selected disabled>{{trans('admin.choose.country')}}</option>
                                                                    @foreach($countries as $country)
                                                                        <option value="{{$country->id}}">{{$country->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.city')}}</label>
                                                                <select name="city_id" class="form-control"
                                                                        id="cities_add">
                                                                    <option selected disabled>{{trans('admin.choose.country.first')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputPassword1">{{trans('admin.address')}}</label>
                                                                <input type="text" class="form-control"
                                                                       id="exampleInputEmail15" placeholder="{{trans('admin.address')}}"
                                                                       name="address" value="{{old('address')}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.gender')}}</label>
                                                                <select name="gender" class="form-control"
                                                                        id="">
                                                                    <option selected disabled>{{trans('admin.choose.gender')}}</option>

                                                                    <option value="m">{{trans('admin.male')}}</option>
                                                                    <option value="f">{{trans('admin.female')}}</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.dob')}}</label>
                                                                <input type="date" class="form-control"
                                                                       id="exampleInputText1"
                                                                       placeholder="dd/mm/year" name="dob"
                                                                       value="{{old('dob')}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputPassword1">{{trans('admin.password')}}</label>
                                                                <input type="password" autocomplete="off" class="form-control"
                                                                       id="exampleInputPassword1" placeholder="{{trans('admin.password')}}"
                                                                       name="password">
                                                            </div>
                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">{{trans('admin.cancel')}}
                                                                </button>
                                                                <button type="submit" class="btn btn-primary">{{trans('admin.confirm')}}

                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- widget content -->
                                        <div class="widget-body p-0">
                                            <table id="dt-basic"
                                                   class="table table-striped table-bordered table-hover"
                                                   width="100%">
                                                <thead>
                                                <tr>
                                                    <th data-hide="phone">{{trans('admin.id')}}</th>
                                                    <th data-class="expand">{{trans('admin.name')}}</th>
                                                    <th data-hide="phone">{{trans('admin.email')}}</th>
                                                    <th data-hide="phone">{{trans('admin.phone')}}</th>
                                                    <th data-hide="phone">{{trans('admin.phone')}}</th>
                                                    <th data-hide="phone">{{trans('admin.city')}}</th>
                                                    <th data-hide="phone">{{trans('admin.dob')}}</th>
                                                    <th data-hide="phone">{{trans('admin.gender')}}</th>
                                                    <th data-hide="phone">{{trans('admin.photo')}}</th>
                                                    <th data-hide="phone">{{trans('admin.address')}}</th>

                                                    <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($users as $user)
                                                    <tr>
                                                        <td>{{$user->id}}</td>
                                                        <td>{{$user->name}}</td>
                                                        <td>{{$user->email}}</td>
                                                        <td>{{$user->phone}}</td>
                                                        <td>{{$user->city ? $user->city->country->name : ''}}</td>
                                                        <td>{{$user->city ? $user->city->name : ''}}</td>
                                                        <td>{{$user->dob}}</td>
                                                        <td>{{$user->gender}}</td>
                                                        <td>
                                                            <div class="superbox-list superbox-8">
                                                                <img src="{{asset('uploads/avatars/'.$user->img)}}"
                                                                     data-img="{{asset('uploads/avatars/'.$user->img)}}"
                                                                     alt="" title="" class="superbox-img"
                                                                     style="height: 73px;width: 118px;">
                                                            </div>
                                                        </td>
                                                        <td>{{$user->address}}</td>

                                                        <td>
                                                            <button class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#myModal-2{{$user->id}}"
                                                                    style="margin-bottom:10px;"><i
                                                                        class="fa fa-edit"></i></button>
                                                            <button class="btn btn-danger" data-toggle="modal"
                                                                    data-target="#myModal-3{{$user->id}}"
                                                                    style="margin-bottom:10px;"><i
                                                                        class="fa fa-trash"></i></button>

                                                        </td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </article>
                            <!-- WIDGET END -->

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->
                </div>
            </div>
        </div>
    </div>
    @foreach($users as $user)

        <!-- Modal  to Edit user-->
        <div class="modal fade" id="myModal-2{{$user->id}}" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"
                            style="font-weight:bold">{{trans('admin.add')}}</h5>
                        <button type="button" class="close" data-dismiss="modal"
                                aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="text-align:left;">
                        <form action="{{route('admin.users.edit',['user_id'=>$user->id])}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">{{trans('admin.name')}}</label>
                                <input type="text" class="form-control"
                                       id="exampleInputText1" placeholder="{{trans('admin.name')}}"
                                       name="name" value="{{$user->name}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">{{trans('admin.email')}}</label>
                                <input type="Email" class="form-control"
                                       id="exampleInputEmail166" placeholder="{{trans('admin.email')}}"
                                       name="email" value="{{$user->email}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">{{trans('admin.phone')}}</label>
                                <input type="number" class="form-control"
                                       id="exampleInputEmail101" placeholder="{{trans('admin.phone')}}<"
                                       name="phone" value="{{$user->phone}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">{{trans('admin.country')}}</label>
                                <select name="country" class="form-control country_edit" uid="{{$user->id}}"
                                        >
                                    <option selected disabled> {{trans('admin.country')}}</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}"
                                                @if ($user->city)
                                                @if ($user->city->country->id == $country->id )
                                                selected
                                                @endif
                                                @endif
                                        >{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">{{trans('admin.city')}}</label>
                                <select name="city_id" class="form-control"
                                        id="cities_edit_{{$user->id}}">
                                    @if ($user->city)
                                        @foreach($user->city->country->cities as $city)
                                            <option value="{{$city->id}}"
                                            @if ($city->id == $user->city_id)
                                                selected
                                            @endif
                                            >{{$city->name}}</option>
                                        @endforeach
                                    @else
                                        <option selected disabled>{{trans('admin.choose.country.first')}}</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">{{trans('admin.address')}}</label>
                                <input type="text" class="form-control"
                                       id="exampleInputEmail127" placeholder="{{trans('admin.address')}}"
                                       name="address" value="{{$user->address}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">{{trans('admin.gender')}}</label>
                                <select name="gender" class="form-control"
                                        id="">
                                    <option selected disabled>{{trans('admin.choose.gender')}}</option>
                                    <option @if ($user->gender == 'm')
                                        selected
                                    @endif value="m">male</option>
                                    <option @if ($user->gender == 'f')
                                        selected
                                                @endif value="f">female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">{{trans('admin.dob')}}</label>
                                <input type="date" class="form-control"
                                       id="exampleInputText11"
                                       placeholder="dd/mm/year" name="dob"
                                       value="{{$user->dob}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">{{trans('admin.password')}}</label>
                                <input type="password" autocomplete="off" class="form-control"
                                       id="exampleInputPassword1" placeholder="{{trans('admin.password')}}"
                                       name="password">
                            </div>
                            <div class="modal-footer" style="margin-top:1rem;">
                                <button type="button" class="btn btn-default"
                                        data-dismiss="modal">{{trans('admin.cancel')}}
                                </button>
                                <button type="submit" class="btn btn-primary">{{trans('admin.confirm')}}

                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <!-- Modal to delet supervisor -->
        <div class="modal fade" id="myModal-3{{$user->id}}" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel"
                            style="font-weight:bold">{{trans('admin.delete')}}</h5>
                        <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"
                         style="text-align:left;">
                        {{trans('admin.delete.confirm')}}
                    </div>
                    <form action="{{route('admin.users.delete',['user_id'=>$user->id])}}" method="post">
                        @csrf
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">{{trans('admin.cancel')}}
                        </button>
                        <button type="submit"
                                class="btn btn-primary">{{trans('admin.confirm')}}
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@stop

@section('scripts')
    <script>
        $(document).on('change', '#country_add', function () {
            let countryID = $(this).val();
            $.ajax({
                url: '{{route('admin.users.get.cities')}}',
                type: 'get',
                dataType: 'json',
                data: {id: countryID, _token: '{{csrf_token()}}'},
                success: function (data) {
                    if (data.status === '1') {
                        let cities = $('#cities_add');
                        cities.html('');
                        $.each(data.cities, function (index, value) {
                            cities.append('<option value="' + value.id + '">' + value.name + '</option>');
                        })
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'حدث خطأ',
                        });
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        });
        $(document).on('change','.country_edit',function () {
            let id = $(this).attr('uid');
            $.ajax({
                url: '{{route('admin.users.get.cities')}}',
                type: 'get',
                dataType: 'json',
                data: {id: id, _token: '{{csrf_token()}}'},
                success: function (data) {
                    if (data.status === '1') {
                        let cities = $('#cities_edit_' + id );
                        cities.html('');
                        $.each(data.cities, function (index, value) {
                            cities.append('<option value="' + value.id + '">' + value.name + '</option>');
                        })
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'حدث خطأ',
                        });
                    }
                },
                error: function () {
                    alert('error');
                }
            });
        })
    </script>
@stop