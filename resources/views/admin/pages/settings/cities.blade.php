@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.settings')}}

@stop

@section('content')

    <div class="sa-content-wrapper">

        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>> {{trans('admin.settings')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>

                    <div class="tab-pane" id="City" role="tabpanel">
                        <!-- widget div-->
                        <div style="text-align: center;">
                            <a style=" margin-top: 1rem;margin-bottom: 1rem;" href="javascript:void(0);"
                               class="btn sa-btn-primary" data-toggle="modal"
                               data-target="#myModal-coun"> Add Province</a>

                            <a class="btn sa-btn-primary" style=" margin-top: 1rem;margin-bottom: 1rem;" href="{{ URL::previous() }}"> Back</a>

                            <!-- Modal  to add supervisor-->
                            <div class="modal fade" id="myModal-coun" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"
                                                style="font-weight:bold">Add
                                                Province</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body" style="text-align:left;">
                                            <form action="{{route('admin.settings.add.city')}}"
                                                  method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">Arabic Province Name</label>
                                                    <input name="name_ar" type="text" class="form-control"
                                                           id="exampleInputText1" placeholder="Province Name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">English Province Name</label>
                                                    <input name="name_en" type="text" class="form-control"
                                                           id="exampleInputText1" placeholder="Province Name">
                                                </div>

                                                <input type="hidden" name="province_id" value="{{$id}}">

                                                <div class="modal-footer" style="margin-top:1rem;">
                                                    <button type="button" class="btn btn-default"
                                                            data-dismiss="modal">Close</button>
                                                    <button type="submit"
                                                            class="btn btn-primary">Save
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- widget content -->
                            <div class="widget-body p-0">


                                <table id="datatable_tabletools"
                                       class="table table-striped table-bordered table-hover"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th data-hide="phone">ID</th>
                                        <th data-class="expand"> Arabic Province Name</th>
                                        <th data-class="expand"> English Province Name</th>

                                        <th data-hide="phone">State</th>

                                        <th data-hide="phone,tablet">Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($cities as $city)

                                        <tr>
                                            <td>{{$city->id}}</td>
                                            <td>{{$city->name_ar}}</td>
                                            <td>{{$city->name_en}}</td>
                                            <td>
                                                <a href="{{route('admin.settings.areas',['id'=>$city->id])}}">show</a>
                                            </td>

                                            <td>
                                                <button class="btn btn-primary" data-toggle="modal"
                                                        data-target="#myModal-2{{$city->id}}" style="margin-bottom:10px;"><i
                                                            class="fa fa-edit"></i></button>
                                                <button class="btn btn-danger" data-toggle="modal"
                                                        data-target="#myModal-3{{$city->id}}" style="margin-bottom:10px;"><i
                                                            class="fa fa-trash"></i></button>

                                            </td>

                                        </tr>

                                    @endforeach

                                    @foreach($cities as $city)


                                        <!-- Modal  to Edit supervisor-->
                                        <div class="modal fade" id="myModal-2{{$city->id}}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel"
                                                            style="font-weight:bold">Edit
                                                            Country</h5>
                                                        <button type="button" class="close"
                                                                data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.settings.edit.city',['id'=>$city->id])}}"
                                                          method="post" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="modal-body"
                                                             style="text-align:left;">
                                                            <div class="form-group">
                                                                <label
                                                                        for="exampleInputEmail1">Arabic Country Name</label>
                                                                <input type="text" name="name_ar" value="{{$city->name_ar}}"
                                                                       class="form-control"
                                                                       id="exampleInputText1"
                                                                       placeholder="Country Name">
                                                            </div>

                                                            <div class="form-group">
                                                                <label
                                                                        for="exampleInputEmail1">English Country Name</label>
                                                                <input type="text" name="name_en" value="{{$city->name_en}}"
                                                                       class="form-control"
                                                                       id="exampleInputText1"
                                                                       placeholder="Country Name">
                                                            </div>

                                                            <div class="modal-footer"
                                                                 style="margin-top:1rem;">
                                                                <button type="button"
                                                                        class="btn btn-default"
                                                                        data-dismiss="modal">Close</button>
                                                                <button type="submit"
                                                                        class="btn btn-primary">Save
                                                                    changes</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>
                                        </div>

                                        <!-- Modal to delet supervisor -->
                                        <div class="modal fade" id="myModal-3{{$city->id}}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel"
                                             aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel"
                                                            style="font-weight:bold">Delet
                                                            Country</h5>
                                                        <button type="button" class="close"
                                                                data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <form action="{{route('admin.settings.delete.city',['id'=>$city->id])}}"
                                                          method="post">
                                                        @csrf
                                                        <div class="modal-body"
                                                             style="text-align:left;">
                                                            Are you sure you want to <strong>Delet This
                                                                Country </strong> ?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button"
                                                                    class="btn btn-default"
                                                                    data-dismiss="modal">No</button>
                                                            <button type="submit"
                                                                    class="btn btn-primary">Yes</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->
                    </div>

                    @stop

                </div>
            </div>
        </div>

    </div>

@section('scripts')
    <script>
        $('#tabs').tabs();
        // Dynamic tabs
        var tabTitle = $("#tab_title"), tabContent = $("#tab_content"),
            tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:5px; left:5px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>",
            tabCounter = 2;

        var tabs = $("#tabs2").tabs();

        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen: false,
            width: 600,
            resizable: false,
            modal: true,
            buttons: [{
                html: "<i class='fa fa-times'></i>&nbsp; Cancel",
                "class": "btn btn-default",
                click: function () {
                    $(this).dialog("close");

                }
            }, {

                html: "<i class='fa fa-plus'></i>&nbsp; Add",
                "class": "btn sa-btn-danger",
                click: function () {
                    addTab();
                    $(this).dialog("close");
                }
            }]
        });
        // addTab form: calls addTab function on submit and closes the dialog
        var form = dialog.find("form").submit(function (event) {
            addTab();
            dialog.dialog("close");
            event.preventDefault();
        });

        // actual addTab function: adds new tab using the input from the form above

    </script>

    <script>
        $(document).on("change", ".status", function () {
            var status = $(this).val();
            var id = $(this).attr("uid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('admin.settings.edit.status') }}",
                type: "post",
                dataType: "json",
                data: {status: status, id: id, _token: token},
                success: function (data) {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        width: '40rem',
                        customClass: "right-check",
                        timer: 1500
                    });
                    if (data.status !== "1") {
                        Swal.fire({
                            type: 'error',
                            title: 'حدث خطأ',
                        });
                    }
                },
                error: function () {
                    Swal.fire({
                        type: 'error',
                        title: 'حدث خطأ',
                    });
                }
            })
        })
    </script>

@stop