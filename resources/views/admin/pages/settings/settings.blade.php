@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.settings')}}

@stop

@section('content')
    <div class="sa-content-wrapper">

        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>> {{trans('admin.settings')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>

                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="well well-sm well-light">

                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="tab" href="#Details" role="tab">Site Details</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#media" role="tab">Social media</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#About" role="tab">About Site</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Team" role="tab">Team Work</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Department" role="tab"> Gallary Department</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Gallary" role="tab">Gallary</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Articles" role="tab">Articles</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Services" role="tab">Services</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Time" role="tab">Time</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Country" role="tab">Country</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Color" role="tab">Color</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Web" role="tab">Web Slider</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Mobile" role="tab">Mobile Slider</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#Testimonial" role="tab">Testimonial</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="tab" href="#clogo" role="tab">Customer Logo</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="Details" role="tabpanel">
                                            <form action="{{route('admin.settings.edit.details')}}" method="post" style="margin-top:1rem" enctype="multipart/form-data">

                                                @csrf
                                                <div class="row">

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Websit Arabic Name</label>
                                                        <input type="text" name="site_name" class="form-control" id="exampleInputText1" placeholder="Websit Arabic Name" value="{{$site_details->site_name}}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Websit Englis Name</label>
                                                        <input name="site_name_en" type="text" class="form-control" id="exampleInputText1" placeholder="Websit Englis Name" value="{{$site_details->site_name_en}}">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Arabic Address</label>
                                                        <input name="address" type="text" class="form-control" id="exampleInputText1" placeholder="Arabic Address" value="{{$site_details->address}}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">English Address</label>
                                                        <input  name="address_en" type="text" class="form-control" id="exampleInputText1" placeholder="English Address" value="{{$site_details->address_en}}">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Phone </label>
                                                        <input name="phone" type="number" class="form-control" id="exampleInputText1" placeholder="Phone" value="{{$site_details->phone}}">
                                                    </div>

                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Email</label>
                                                        <input name="email" type="email" class="form-control" id="exampleInputText1" placeholder="Email" value="{{$site_details->email}}">
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleInputEmail1">Map</label>
                                                        <input name="map" type="text" class="form-control" id="exampleInputText1" placeholder="https://www.google.com/maps" value="{{$site_details->map}}" >
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.site.logo')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="logo" id="logo"
                                                                   accept=".gif, .jpg, .png"/>
                                                            <label for="logo" class="file-ok"
                                                                   style="background-image: url({{$site_details->logo ?  url('uploads/site_details/'.$site_details->logo) : asset('default-logo.jpg')}});">
                                                            <span><i class="fa fa-file-image-o "
                                                                     style="font-size:5rem;"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>{{trans('admin.site.icon')}}</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="icon" id="icon"
                                                                   accept=".gif, .jpg, .png"/>
                                                            <label for="icon" class="file-ok"
                                                                   style="background-image: url({{$site_details->icon ?  url('uploads/site_details/'.$site_details->icon) : asset('default-logo.jpg')}});">
                                                            <span><i class="fa fa-file-image-o "
                                                                     style="font-size:5rem;"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer" style="margin-top:1rem;">
                                                    <button type="button" class="btn btn-default">Close</button>
                                                    <button type="submit" class="btn btn-primary">{{trans('admin.confirm')}}</button>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="media" role="tabpanel">
                                            <form action="{{route('admin.settings.edit.social')}}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.facebook')}}<i
                                                                class="fa fa-facebook-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.facebook')}}" name="facebook"
                                                           value="{{$social->facebook}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.twitter')}} <i
                                                                class="fa fa-twitter-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.twitter')}}" name="twitter"
                                                           value="{{$social->twitter}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.instagram')}} <i
                                                                class="fa fa-instagram"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.instagram')}}" name="instagram"
                                                           value="{{$social->instagram}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.youtube')}} <i
                                                                class="fa fa-youtube-play"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.youtube')}}" name="youtube"
                                                           value="{{$social->youtube}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.snapchat')}} <i
                                                                class="fa fa-snapchat-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.snapchat')}}" name="snapchat"
                                                           value="{{$social->snapchat}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.whatsapp')}} <i
                                                                class="fa fa-whatsapp"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.whatsapp')}} " name="whatsapp"
                                                           value="{{$social->whatsapp}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.google.plus ')}} <i
                                                                class="fa fa-google-plus-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.google.plus ')}}" name="google_plus"
                                                           value="{{$social->google_plus}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.linkedin ')}} <i
                                                                class="fa fa-linkedin-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.linkedin ')}}" name="linked"
                                                           value="{{$social->linked}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1">{{trans('admin.pinterest ')}} <i
                                                                class="fa fa-pinterest-square"></i></label>
                                                    <input type="text" class="form-control" id="exampleInputText1"
                                                           placeholder="{{trans('admin.pinterest ')}}" name="pinterest"
                                                           value="{{$social->pinterest}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary">{{trans('admin.confirm')}}
                                                    </button>
                                                    <button type="button" class="btn btn-default">Close</button>

                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="About" role="tabpanel">
                                            <form style="margin-top:1rem" action="{{route('admin.settings.edit.about')}}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>About Us Image1</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img1" id="img1" accept=".gif, .jpg, .png" />
                                                            <label for="img1" class="file-ok" style="background-image: url({{$about->img1 ?  url('uploads/site_about/'. $about->img1) : asset('default-logo.jpg')}});">
                                                                <span>Upload image</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>About Us Image2</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img2" id="img2" accept=".gif, .jpg, .png" />
                                                            <label for="img2" class="file-ok" style="background-image: url({{$about->img2 ?  url('uploads/site_about/'. $about->img2) : asset('default-logo.jpg')}});">
                                                                <span>Upload image</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>About Us Image3</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img3" id="img3" accept=".gif, .jpg, .png" />
                                                            <label for="img3" class="file-ok" style="background-image: url({{$about->img3 ?  url('uploads/site_about/'. $about->img3) : asset('default-logo.jpg')}});">
                                                                <span>Upload image</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                            <label>About Us Image4</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img4" id="img4" accept=".gif, .jpg, .png" />
                                                            <label for="img4" class="file-ok" style="background-image: url({{$about->img4 ?  url('uploads/site_about/'. $about->img4) : asset('default-logo.jpg')}});">
                                                                <span>Upload image</span>
                                                            </label>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                            <label>About Us Image5</label>
                                                        </div>
                                                        <div class="wrap-custom-file ">
                                                            <input type="file" name="img5" id="img5" accept=".gif, .jpg, .png" />
                                                            <label for="img5" class="file-ok" style="background-image: url({{$about->img5 ?  url('uploads/site_about/'. $about->img5) : asset('default-logo.jpg')}});">
                                                                <span>Upload image</span>
                                                            </label>
                                                        </div>
                                                        </div>
                                                            </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">Arabic Title</label>
                                                        <input type="text" name="title" value="{{$about->title}}" class="form-control" id="exampleInputText1" placeholder="Arabic Title">
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputEmail1">English Title</label>
                                                        <input type="text" name="title_en" class="form-control" value="{{$about->title_en}}" id="exampleInputText1" placeholder="English Title">
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleFormControlTextarea1">About Us In Arabic</label>
                                                        <textarea class="form-control" name="details" id="exampleFormControlTextarea1" rows="3" placeholder="About Us In Arabic">{{$about->details}}</textarea>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label for="exampleFormControlTextarea1">About Us In English</label>
                                                        <textarea class="form-control" name="details_en" id="exampleFormControlTextarea1" rows="3" placeholder="About Us In English">{{$about->details_en}}</textarea>
                                                    </div>
                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                        <button type="button" class="btn btn-default">Close</button>
                                                        <button type="submit" class="btn btn-primary">Save
                                                            changes</button>
                                                    </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane" id="Team" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-1"> Add Worker</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Worker</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.team')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">

                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Name In Arabic</label>
                                                                                <input type="text" name="name" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Name In English</label>
                                                                                <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                            </div>

                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Job Title In English</label>
                                                                                <input type="text" name="title_en" class="form-control" id="exampleInputText1" placeholder="Job Title In English">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Job Title In Arabic</label>
                                                                                <input type="text"  name="title" class="form-control" id="exampleInputText1" placeholder="Job Title In Arabic">
                                                                            </div>

                                                                            <div class="wrap-custom-file ">
                                                                                <input type="file" name="img" id="image9" accept=".gif, .jpg, .png" />
                                                                                <label for="image9" class="file-ok">
                                                                                    <span>image</span>
                                                                                </label>
                                                                            </div>
                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        @foreach($teams as $team)

                                                            <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Name In Arabic</th>
                                                            <th data-class="expand">Name In English</th>

                                                            <th data-hide="phone">Job Title In Arabic</th>
                                                            <th data-hide="phone">Job Title In English</th>
                                                            <th data-hide="phone">Image</th>

                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        <tr>

                                                            <td>{{$team->id}}</td>
                                                            <td>{{$team->name}}</td>
                                                            <td>{{$team->name_en}}</td>
                                                            <td>{{$team->title}}</td>
                                                            <td>{{$team->title_en}}</td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/team/'. $team->img)}}" data-img="{{asset('uploads/team/'. $team->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job{{$team->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob{{$team->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        @endforeach


                                                        @foreach($teams as $team)
                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-job{{$team->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Edit Details</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.team',['id'=>$team->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In Arabic</label>
                                                                                    <input type="text" name="name" value="{{$team->name}}" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In English</label>
                                                                                    <input type="text" name="name_en" value="{{$team->name_en}}" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Job Title In English</label>
                                                                                    <input type="text" name="title_en" value="{{$team->title_en}}" class="form-control" id="exampleInputText1" placeholder="Job Title In English">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Job Title In Arabic</label>
                                                                                    <input type="text" name="title" value="{{$team->title}}" class="form-control" id="exampleInputText1" placeholder="Job Title In Arabic">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9{{$team->id}}" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9{{$team->id}}"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/team/'.$team->img)}});">
                                                                                        <span>image</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                            <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-deljob{{$team->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.team',['id'=>$team->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Worker </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>

                                        <div class="tab-pane" id="Department" role="tabpanel">
                                            <div style="text-align: center;">

                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom: 1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-depart"> Add Department</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-depart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Department</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.gallery.department')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf                                                                    <div class="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Name Of Department In Englis</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="Name Of Department In English">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Name Of Department In Arabic</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="Name Of Department In Arabic">
                                                                        </div>

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image10"
                                                                                   accept=".gif, .jpg, .png"/>
                                                                            <label for="image10">
                                                                                <span>{{trans('admin.photo')}}</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Save
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">
                                                    <table id="datatable_tabletools1" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-class="expand">Name Of Department In Arabic</th>
                                                            <th data-class="expand">Name Of Department In English</th>
                                                            <th data-hide="phone,tablet">Action</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($gallery_departments as $gallery_department)

                                                            <tr>
                                                            <td>{{$gallery_department->name_ar}}</td>
                                                            <td>{{$gallery_department->name_en}}</td>
                                                            <td><img style="width:50px;height:45px;" src="{{asset('uploads/gallery/' . $gallery_department->img)}}"></td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-depart1{{$gallery_department->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-depart2{{$gallery_department->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-depart1{{$gallery_department->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Edit Details</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.gallery.department',['id'=>$gallery_department->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name Of Department In English</label>
                                                                                    <input type="text" name="name_ar" value="{{$gallery_department->name_ar}}" class="form-control" id="exampleInputText1" placeholder="Name Of Department In English">
                                                                                </div>


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name Of Department In Arabic</label>
                                                                                    <input type="text" name="name_en" value="{{$gallery_department->name_en}}" class="form-control" id="exampleInputText1" placeholder="Name Of Department In Arabic">
                                                                                </div>

                                                                                <div class="wrap-custom-file">
                                                                                    <input type="file" name="img"
                                                                                           id="image11{{$gallery_department->id}}"
                                                                                           accept=".gif, .jpg, .png"/>
                                                                                    <label for="image11{{$gallery_department->id}}"
                                                                                           class="file-ok"
                                                                                           style="background-image: url({{url('uploads/gallery/'.$gallery_department->img)}});">
                                                                                        <span>{{trans('admin.photo')}}</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>
                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-primary">Save
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-depart2{{$gallery_department->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.gallery.department',['id'=>$gallery_department->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">

                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Department </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="Gallary" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-galls"> Add Image</a>

                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-galls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Image</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.gallery')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Name In Arabic</label>
                                                                            <input type="text" name="name" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Name In English</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">Image
                                                                                Text In English</label>
                                                                            <textarea class="form-control" name="text" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">Image
                                                                                Text In English</label>
                                                                            <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>

                                                                        @foreach($gallery_departments as $gallery_department)

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">Image
                                                                                Department Arabic </label>
                                                                            <select name="department_ar" class="form-control">
                                                                                <option value="{{$gallery_department->name_ar}}">{{$gallery_department->name_ar}}</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">Image
                                                                                Department English</label>
                                                                            <select name="department_en" class="form-control">
                                                                                <option value="{{$gallery_department->name_en}}">{{$gallery_department->name_en}}</option>
                                                                            </select>
                                                                        </div>

                                                                        @endforeach

                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-g" accept=".gif, .jpg, .png" />
                                                                            <label for="image-g">
                                                                                <span>Uplaod Image</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Name In Arabic</th>
                                                            <th data-class="expand">Name In English</th>

                                                            <th data-hide="phone">Text In Arabic</th>
                                                            <th data-hide="phone">Text In English</th>
                                                            <th data-hide="phone">Department Image Arabic</th>
                                                            <th data-hide="phone">Department Image English</th>
                                                            <th data-hide="phone">Image</th>

                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($galleries as $gallery)

                                                            <tr>
                                                            <td>{{$gallery->id}}</td>
                                                            <td>{{$gallery->name}}</td>
                                                            <td>{{$gallery->name_en }}</td>

                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-gall2{{$gallery->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-gall5{{$gallery->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                {{$gallery->department_ar}}
                                                            </td>
                                                            <td>
                                                                {{$gallery->department_en}}
                                                            </td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/gallery/' . $gallery->img)}}" data-img="{{asset('uploads/gallery/' . $gallery->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-gall3{{$gallery->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-gall4{{$gallery->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                            @endforeach

                                                        @foreach($galleries as $gallery)


                                                            <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-gall5{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text Arabic</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$gallery->text}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-gall2{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text English</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$gallery->text_en}}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-gall3{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Edit
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.gallery',['id'=>$gallery->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In Arabic</label>
                                                                                    <input type="text" name="name" value="{{$gallery->name}}" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                                </div>

                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In English</label>
                                                                                    <input type="text" name="name_en" value="{{$gallery->name_en}}" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">Image
                                                                                        Text In English</label>
                                                                                    <textarea class="form-control" name="text" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3">{{$gallery->text}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-12">
                                                                                    <label for="exampleFormControlTextarea1">Image
                                                                                        Text In Arabic</label>
                                                                                    <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" placeholder="Image Text In Arabic" rows="3">{{$gallery->text_en}}</textarea>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">Image
                                                                                        English Department </label>
                                                                                    <select name="department_en" class="form-control">
                                                                                        <option selected disabled>{{$gallery->department_en}}</option>
                                                                                        @foreach($gallery_departments as $gallery_department)
                                                                                            <option value="{{$gallery_department->name_en}}">{{$gallery_department->name_en}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleFormControlTextarea1">Image
                                                                                        Arabic Department </label>
                                                                                    <select name="department_ar" class="form-control">
                                                                                        <option selected disabled>{{$gallery->department_ar}}</option>
                                                                                        @foreach($gallery_departments as $gallery_department)
                                                                                            <option value="{{$gallery_department->name_ar}}">{{$gallery_department->name_ar}}</option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image6-g" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6-g"  class="file-ok"
                                                                                           style="background-image: url({{url('uploads/gallery/'.$gallery->img)}});" >
                                                                                        <span>Uplaod Image</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-gall4{{$gallery->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.gallery',['id'=>$gallery->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Image </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>

                                        <div class="tab-pane" id="Articles" role="tabpanel">
                                            <!-- widget div-->
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-galls-article"> Add Article</a>

                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-galls-article" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Image</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.article')}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Author In Arabic</label>
                                                                            <input type="text" name="author_ar" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Author In English</label>
                                                                            <input type="text" name="author_en" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Date</label>
                                                                            <input type="date" name="date" class="form-control" id="exampleInputText1" placeholder="Date">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Title In Arabic</label>
                                                                            <input type="text" name="title_ar" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                        </div>

                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Title In English</label>
                                                                            <input type="text" name="title_en" class="form-control" id="exampleInputText1" placeholder="Name In English">
                                                                        </div>

                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">Content In Arabic</label>
                                                                            <textarea class="form-control" name="content_ar" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>
                                                                        <div class="form-group col-md-12">
                                                                            <label for="exampleFormControlTextarea1">Image
                                                                                Content In English</label>
                                                                            <textarea class="form-control" name="content_en" id="exampleFormControlTextarea1" placeholder="Image Text In English" rows="3"></textarea>
                                                                        </div>


                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image-a" accept=".gif, .jpg, .png" />
                                                                            <label for="image-a">
                                                                                <span>Uplaod Image</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="widget-body p-0">

                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Arabic Author Name</th>
                                                            <th data-class="expand">English Author Name</th>
                                                            <th data-hide="phone">Date</th>
                                                            <th data-hide="phone">Arabic Article Title</th>
                                                            <th data-hide="phone">English Article Title</th>
                                                            <th data-hide="phone">Arabic Article Content</th>
                                                            <th data-hide="phone">Arabic Article Content</th>
                                                            <th data-hide="phone">Image</th>


                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($articles as $article)

                                                        <tr>
                                                            <td>{{$article->id}}</td>
                                                            <td>{{$article->author_ar}}</td>
                                                            <td>{{$article->author_en}}</td>
                                                            <td>{{$article->date}}</td>
                                                            <td>{{$article->title_ar}}</td>
                                                            <td>{{$article->title_en}}</td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-7{{$article->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-info" data-toggle="modal" data-target="#myModal-artic2{{$article->id}}" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>


                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/article/' . $article->img)}}" data-img="{{asset('uploads/article/' . $article->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-6{{$article->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-5{{$article->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        @endforeach

                                                        @foreach($articles as $article)

                                                            <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-artic2{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Article Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$article->content_en}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-7{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Article Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> {{$article->content_ar}}</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-6{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Edit
                                                                                Image</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.article',['id'=>$article->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Author In Arabic</label>
                                                                                        <input type="text" name="author_ar" value="{{$article->author_ar}}" class="form-control" id="exampleInputText1" placeholder="Author In Arabic">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Author In English</label>
                                                                                        <input type="text" name="author_en" value="{{$article->author_en}}" class="form-control" id="exampleInputText1" placeholder="Author In English">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Date</label>
                                                                                        <input type="date" name="date" value="{{$article->date}}" class="form-control" id="exampleInputText1" placeholder="Date">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Author In Arabic</label>
                                                                                        <input type="text" name="title_ar" value="{{$article->title_ar}}" class="form-control" id="exampleInputText1" placeholder="Title In Arabic">
                                                                                    </div>

                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Author In English</label>
                                                                                        <input type="text" name="title_en" value="{{$article->title_en}}" class="form-control" id="exampleInputText1" placeholder="Title In English">
                                                                                    </div>

                                                                                    <div class="form-group col-md-12">
                                                                                        <label for="exampleFormControlTextarea1">Content In Arabic</label>
                                                                                        <textarea class="form-control" name="content_ar" id="exampleFormControlTextarea1" placeholder="Content In Arabic" rows="3">{{$article->content_ar}}</textarea>
                                                                                    </div>
                                                                                    <div class="form-group col-md-12">
                                                                                        <label for="exampleFormControlTextarea1">Content In English</label>
                                                                                        <textarea class="form-control" name="content_en" id="exampleFormControlTextarea1" placeholder="Content In English" rows="3">{{$article->content_en}}</textarea>
                                                                                    </div>

                                                                                    <div class="wrap-custom-file ">
                                                                                        <input type="file" name="img" id="image6-a" accept=".gif, .jpg, .png" />
                                                                                        <label for="image6-a"  class="file-ok"
                                                                                               style="background-image: url({{url('uploads/article/'.$article->img)}});" >
                                                                                            <span>Uplaod Image</span>
                                                                                        </label>
                                                                                    </div>

                                                                                </div>


                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary">Save
                                                                                        changes</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <!-- Modal to delet Article -->
                                                        <div class="modal fade" id="myModal-5{{$article->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Article</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.article',['id'=>$article->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to <strong>Delet This
                                                                            Articles </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->
                                        </div>
                                        <div class="tab-pane" id="Services" role="tabpanel" style="text-align:center;">
                                            <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                               data-target="#myModal-addserv"> Add Service</a>
                                            <!-- Modal  to Edit image-->
                                            <div class="modal fade" id="myModal-addserv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                Service</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body" style="text-align:left;">
                                                            <form action="{{route('admin.settings.add.service')}}"
                                                                  method="post" enctype="multipart/form-data">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">Arabic Name</label>
                                                                    <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="Arabic Name">
                                                                </div>

                                                                <div class="form-group">
                                                                    <label for="exampleInputEmail1">English Name</label>
                                                                    <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="English Name">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlTextarea1">
                                                                        Text Arabic</label>
                                                                    <textarea class="form-control" name="text_ar" id="exampleFormControlTextarea1" placeholder="Image Text Arabic" rows="3"></textarea>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlTextarea1">
                                                                        Text English</label>
                                                                    <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" placeholder="Image Text English" rows="3"></textarea>
                                                                </div>
                                                                <div class="wrap-custom-file ">
                                                                    <input type="file" name="img" id="image5-s" accept=".gif, .jpg, .png" />
                                                                    <label for="image5-s">
                                                                        <span><i class="fa fa-file-image-o " style="font-size:5rem;"></i></span>
                                                                    </label>
                                                                </div>

                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="submit" class="btn btn-primary">Save
                                                                        changes</button>
                                                                </div>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                <thead>
                                                <tr>
                                                    <th data-hide="phone">ID</th>
                                                    <th data-class="expand">Arabic Name</th>
                                                    <th data-class="expand">English Name</th>


                                                    <th data-hide="phone">Arabic Text</th>
                                                    <th data-hide="phone">English Text</th>
                                                    <th data-hide="phone">Image</th>

                                                    <th data-hide="phone,tablet">Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($services as $service)
                                                <tr>

                                                    <td>{{$service->id}}</td>

                                                    <td>{{$service->name_ar}}</td>
                                                    <td>{{$service->name_en}}</td>
                                                    <td>
                                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModel-ar{{$service->id}}" style="margin-top:10px;color:#fff">Show
                                                            Details</button>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModel-en{{$service->id}}" style="margin-top:10px;color:#fff">Show
                                                            Details</button>
                                                    </td>
                                                    <td>
                                                        <div class="superbox-list superbox-8">
                                                            <img src="{{asset('uploads/services/' . $service->img)}}" data-img="{{asset('uploads/service/' . $service->img)}}"
                                                                 alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                        </div>
                                                    </td>


                                                    <td>
                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModel-2{{$service->id}}" style="margin-bottom:10px;"><i
                                                                    class="fa fa-edit"></i></button>
                                                        <button class="btn btn-danger" data-toggle="modal" data-target="#myModel-1{{$service->id}}" style="margin-bottom:10px;"><i
                                                                    class="fa fa-trash"></i></button>

                                                    </td>

                                                </tr>

                                                @endforeach

                                                @foreach($services as $service)

                                                    <!-- Modal  to show image-->
                                                <div class="modal fade" id="myModel-ar{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                     Text Arabic</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <p> {{$service->text_ar}}</p>
                                                            </div>
                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Modal  to show image-->
                                                <div class="modal fade" id="myModel-en{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                    Text Arabic</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <p> {{$service->text_en}}</p>
                                                            </div>
                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- Modal  to Edit image-->
                                                <div class="modal fade" id="myModel-2{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Edit
                                                                    Service</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.edit.service',['id'=>$service->id])}}"
                                                                      method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Arabic Name</label>
                                                                        <input type="text" name="name_ar" value="{{$service->name_ar}}" class="form-control" id="exampleInputText1" placeholder="Arabic Name">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">English Name</label>
                                                                        <input type="text" name="name_en" value="{{$service->name_en}}" class="form-control" id="exampleInputText1" placeholder="English Name">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlTextarea1">
                                                                            Text Arabic</label>
                                                                        <textarea class="form-control" name="text_ar" id="exampleFormControlTextarea1" placeholder="Text Arabic" rows="3">{{$service->text_ar}}</textarea>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleFormControlTextarea1">
                                                                            Text English</label>
                                                                        <textarea class="form-control" name="text_en" id="exampleFormControlTextarea1" placeholder="Text English" rows="3">{{$service->text_en}}</textarea>
                                                                    </div>
                                                                    <div class="wrap-custom-file ">
                                                                        <input type="file" name="img" id="image5-s{{$service->id}}" accept=".gif, .jpg, .png" />
                                                                        <label for="image5-s{{$service->id}}" class="file-ok"
                                                                               style="background-image: url({{url('uploads/services/'.$service->img)}});">
                                                                            <span><i class="fa fa-file-image-o " style="font-size:5rem;"></i></span>
                                                                        </label>
                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>



                                                <!-- Modal to delet image -->
                                                <div class="modal fade" id="myModel-1{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                    Image</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{route('admin.settings.delete.service',['id'=>$service->id])}}"
                                                                  method="post">
                                                                @csrf
                                                            <div class="modal-body" style="text-align:left;">
                                                                Are you sure you want to
                                                                <strong>Delet This
                                                                    Image </strong> ?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                <button type="submit" class="btn btn-primary">Yes</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>

                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="Time" role="tabpanel">
                                            <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%" style="margin-top:1rem">
                                                <thead>
                                                @foreach($work_days as $day)
                                                <tr>
                                                    <th>Day</th>
                                                    <th>Time</th>


                                                    <th>status</th>


                                                    <th>Action</th>

                                                </tr>
                                                </thead>
                                                <tbody>


                                                <tr>
                                                    <td>{{$day->day}}</td>
                                                    <td> {{$day->from}} {{$day->from_timing}}
                                                        : {{$day->to}} {{$day->to_timing}}</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select class="form-control status"
                                                                    uid="{{ $day->id }}">
                                                                <option @if($day->is_active) selected
                                                                        @endif value="1">
                                                                    {{trans('admin.active')}}
                                                                </option>
                                                                <option @if(!$day->is_active) selected
                                                                        @endif value="0">
                                                                    {{trans('admin.pending')}}
                                                                </option>
                                                            </select>

                                                        </div>
                                                    </td>

                                                    <td>
                                                        <button class="btn btn-primary" type="button"
                                                                data-toggle="modal"
                                                                data-target="#exampleModalCenter{{$day->id}}"
                                                                style="margin-bottom:10px;"><i
                                                                    class="fa fa-edit"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @endforeach

                                                @foreach($work_days as $day)

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalCenter{{$day->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Edit workdays</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{route('admin.settings.edit.time',['id'=>$day->id])}}"
                                                                  method="post" enctype="multipart/form-data">
                                                                @csrf
                                                            <div class="modal-body">

                                                                <div class="row">

                                                                    <div class="col-lg-6 col-sm-12 col-xs-12 ">
                                                                        <h5>from:</h5>
                                                                        <div style="float:left" class="form-group">

                                                                            <select name="from" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="1" @if ($day->from == 1)
                                                                                selected
                                                                                        @endif>1
                                                                                </option>
                                                                                <option value="2" @if ($day->from == 2)
                                                                                selected
                                                                                        @endif>2
                                                                                </option>
                                                                                <option value="3" @if ($day->from == 3)
                                                                                selected
                                                                                        @endif>3
                                                                                </option>
                                                                                <option value="4" @if ($day->from == 4)
                                                                                selected
                                                                                        @endif>4
                                                                                </option>
                                                                                <option value="5" @if ($day->from == 5)
                                                                                selected
                                                                                        @endif>5
                                                                                </option>
                                                                                <option value="6" @if ($day->from == 6)
                                                                                selected
                                                                                        @endif>6
                                                                                </option>
                                                                                <option value="7" @if ($day->from == 7)
                                                                                selected
                                                                                        @endif>7
                                                                                </option>
                                                                                <option value="8" @if ($day->from == 8)
                                                                                selected
                                                                                        @endif>8
                                                                                </option>
                                                                                <option value="9" @if ($day->from == 9)
                                                                                selected
                                                                                        @endif>9
                                                                                </option>
                                                                                <option value="10" @if ($day->from == 10)
                                                                                selected
                                                                                        @endif>10
                                                                                </option>
                                                                                <option value="11" @if ($day->from == 11)
                                                                                selected
                                                                                        @endif>11
                                                                                </option>
                                                                                <option value="12" @if ($day->from == 12)
                                                                                selected
                                                                                        @endif>12
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div style="display:inline-block;margin-left: 20px;" class="form-group">

                                                                            <select name="from_timing" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="am"
                                                                                        @if ($day->from_timing == 'am')
                                                                                        selected
                                                                                        @endif>{{trans('admin.am')}}
                                                                                </option>
                                                                                <option value="pm"
                                                                                        @if ($day->from_timing == 'pm')
                                                                                        selected
                                                                                        @endif>{{trans('admin.pm')}}
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-6 col-sm-12 col-xs-12">
                                                                        <h5>to:</h5>
                                                                        <div style="float:left" class="form-group">

                                                                            <select name="to" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="1" @if ($day->to == 1)
                                                                                selected
                                                                                        @endif>1
                                                                                </option>
                                                                                <option value="2" @if ($day->to == 2)
                                                                                selected
                                                                                        @endif>2
                                                                                </option>
                                                                                <option value="3" @if ($day->to == 3)
                                                                                selected
                                                                                        @endif>3
                                                                                </option>
                                                                                <option value="4" @if ($day->to == 4)
                                                                                selected
                                                                                        @endif>4
                                                                                </option>
                                                                                <option value="5" @if ($day->to == 5)
                                                                                selected
                                                                                        @endif>5
                                                                                </option>
                                                                                <option value="6" @if ($day->to == 6)
                                                                                selected
                                                                                        @endif>6
                                                                                </option>
                                                                                <option value="7" @if ($day->to == 7)
                                                                                selected
                                                                                        @endif>7
                                                                                </option>
                                                                                <option value="8" @if ($day->to == 8)
                                                                                selected
                                                                                        @endif>8
                                                                                </option>
                                                                                <option value="9" @if ($day->to == 9)
                                                                                selected
                                                                                        @endif>9
                                                                                </option>
                                                                                <option value="10" @if ($day->to == 10)
                                                                                selected
                                                                                        @endif>10
                                                                                </option>
                                                                                <option value="11" @if ($day->to == 11)
                                                                                selected
                                                                                        @endif>11
                                                                                </option>
                                                                                <option value="12" @if ($day->to == 12)
                                                                                selected
                                                                                        @endif>12
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                        <div style="display:inline-block;margin-left: 20px;" class="form-group">

                                                                            <select name="to_timing" class="form-control" id="exampleFormControlSelect1">
                                                                                <option value="am"
                                                                                        @if ($day->to_timing == 'am')
                                                                                        selected
                                                                                        @endif>{{trans('admin.am')}}
                                                                                </option>
                                                                                <option value="pm"
                                                                                        @if ($day->to_timing == 'pm')
                                                                                        selected
                                                                                        @endif>{{trans('admin.pm')}}
                                                                                </option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>


                                        <div class="tab-pane" id="Country" role="tabpanel">
                                            <!-- widget div-->
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;margin-bottom: 1rem;" href="javascript:void(0);"
                                                   class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-coun"> Add Country</a>



                                                <!-- Modal  to add supervisor-->
                                                <div class="modal fade" id="myModal-coun" tabindex="-1" role="dialog"
                                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel"
                                                                    style="font-weight:bold">Add
                                                                    Country</h5>
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.country')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Arabic Country Name</label>
                                                                        <input name="name_ar" type="text" class="form-control"
                                                                               id="exampleInputText1" placeholder="Country Name">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">English Country Name</label>
                                                                        <input name="name_en" type="text" class="form-control"
                                                                               id="exampleInputText1" placeholder="Country Name">
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default"
                                                                                data-dismiss="modal">Close</button>
                                                                        <button type="submit"
                                                                                class="btn btn-primary">Save
                                                                        </button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools"
                                                           class="table table-striped table-bordered table-hover"
                                                           width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand"> Arabic Country Name</th>
                                                            <th data-class="expand"> English Country Name</th>

                                                            <th data-hide="phone">State</th>

                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($countries as $country)

                                                        <tr>
                                                            <td>{{$country->id}}</td>
                                                            <td>{{$country->name_ar}}</td>
                                                            <td>{{$country->name_en}}</td>
                                                            <td>
                                                                <a href="{{route('admin.settings.provinces',['id'=>$country->id])}}">show</a>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal"
                                                                        data-target="#myModal-2{{$country->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal"
                                                                        data-target="#myModal-3{{$country->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                        @endforeach

                                                        @foreach($countries as $country)


                                                            <!-- Modal  to Edit supervisor-->
                                                        <div class="modal fade" id="myModal-2{{$country->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLabel"
                                                                            style="font-weight:bold">Edit
                                                                            Country</h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.edit.country',['id'=>$country->id])}}"
                                                                          method="post" enctype="multipart/form-data">
                                                                        @csrf
                                                                    <div class="modal-body"
                                                                         style="text-align:left;">
                                                                            <div class="form-group">
                                                                                <label
                                                                                        for="exampleInputEmail1">Arabic Country Name</label>
                                                                                <input type="text" name="name_ar" value="{{$country->name_ar}}"
                                                                                       class="form-control"
                                                                                       id="exampleInputText1"
                                                                                       placeholder="Country Name">
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label
                                                                                        for="exampleInputEmail1">English Country Name</label>
                                                                                <input type="text" name="name_en" value="{{$country->name_en}}"
                                                                                       class="form-control"
                                                                                       id="exampleInputText1"
                                                                                       placeholder="Country Name">
                                                                            </div>

                                                                            <div class="modal-footer"
                                                                                 style="margin-top:1rem;">
                                                                                <button type="button"
                                                                                        class="btn btn-default"
                                                                                        data-dismiss="modal">Close</button>
                                                                                <button type="submit"
                                                                                        class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                    </div>
                                                                        </form>
                                                                    </div>

                                                            </div>
                                                        </div>

                                                        <!-- Modal to delet supervisor -->
                                                        <div class="modal fade" id="myModal-3{{$country->id}}" tabindex="-1"
                                                             role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title"
                                                                            id="exampleModalLabel"
                                                                            style="font-weight:bold">Delet
                                                                            Country</h5>
                                                                        <button type="button" class="close"
                                                                                data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.country',['id'=>$country->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body"
                                                                         style="text-align:left;">
                                                                        Are you sure you want to <strong>Delet This
                                                                            Country </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button"
                                                                                class="btn btn-default"
                                                                                data-dismiss="modal">No</button>
                                                                        <button type="submit"
                                                                                class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->
                                        </div>

                                        <div class="tab-pane" id="Color" role="tabpanel">
                                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                                 data-widget-editbutton="false" >
                                                <header>
                                                    <div class="widget-header">
																<span class="widget-icon"> <i class="fa fa-lg  fa-globe"></i>
																</span>
                                                        <h2>Web Slider</h2>
                                                    </div>
                                                </header>
                                                <!-- widget div-->
                                                <div style="text-align: center;">
                                                    <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                       data-target="#myModal-colo" > Change Color</a>



                                                    <!-- Modal  to add image-->
                                                    <div class="modal fade" id="myModal-colo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                        Change Color</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body" style="text-align:left;">
                                                                    <form action="{{route('admin.settings.add.web.color')}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="row">


                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">First Color</label>
                                                                                <input type="text" name="first" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Second Color</label>
                                                                                <input type="text" name="second" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Third Color</label>
                                                                                <input type="text" name="third" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Fourth Color</label>
                                                                                <input type="text" name="fourth" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                            </div>
                                                                        </div>

                                                                        <div class="modal-footer" style="margin-top:1rem;">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-primary">Save
                                                                                changes</button>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- widget content -->
                                                    <div class="widget-body p-0">


                                                        <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th data-hide="phone">ID</th>
                                                                <th data-class="expand">First Color</th>
                                                                <th data-class="expand">Second Color</th>

                                                                <th data-class="expand">Third Color</th>
                                                                <th data-class="expand">Fourth Color</th>

                                                                <th data-hide="phone,tablet">Action</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach($web_colors as $web_color)

                                                            <tr>

                                                                <td>{{$web_color->id}}</td>
                                                                <td>{{$web_color->first}}</td>
                                                                <td>{{$web_color->second}}</td>
                                                                <td>{{$web_color->third}}</td>
                                                                <td>{{$web_color->fourth}}</td>

                                                                <td>
                                                                    <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job{{$web_color->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-edit"></i></button>
                                                                    <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob{{$web_color->id}}" style="margin-bottom:10px;"><i
                                                                                class="fa fa-trash"></i></button>

                                                                </td>

                                                            </tr>

                                                            @endforeach

                                                            @foreach($web_colors as $web_color)

                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-job{{$web_color->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                Edit Color</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.web.color',['id'=>$web_color->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">


                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">First Color</label>
                                                                                        <input type="text" name="first" value="{{$web_color->first}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Second Color</label>
                                                                                        <input type="text" name="second" value="{{$web_color->second}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Third Color</label>
                                                                                        <input type="text" name="third" value="{{$web_color->third}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Fourth Color</label>
                                                                                        <input type="text" name="fourth" value="{{$web_color->fourth}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary">Save
                                                                                        changes</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModal-deljob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            </h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.settings.delete.web.color',['id'=>$web_color->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            Are you sure you want to
                                                                            <strong>Delet This
                                                                                Color </strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                            <button type="submit" class="btn btn-primary">Yes</button>
                                                                        </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                                @endforeach

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <!-- end widget content -->

                                                </div>
                                                <!-- end widget div -->

                                            </div>
                                            <!-- end widget -->
                                            <!-- Widget ID (each widget will need unique ID)-->
                                            <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-4"
                                                 data-widget-editbutton="false">
                                                <header>
                                                    <div class="widget-header">
					<span class="widget-icon"> <i class="fa fa-lg  fa-globe"></i>
					</span>
                                                        <h2>Mobile Slider</h2>
                                                    </div>
                                                </header>
                                                <!-- widget div-->
                                                <div style="text-align: center;">
                                                    <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                       data-target="#myModal-1-mob"> Change Color</a>



                                                    <!-- Modal  to add image-->
                                                    <div class="modal fade" id="myModal-1-mob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                         aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                        Change Color</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body" style="text-align:left;">
                                                                    <form action="{{route('admin.settings.add.mobile.color')}}"
                                                                          method="post">
                                                                        @csrf
                                                                        <div class="row">


                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">First Color</label>
                                                                                <input type="text" name="first" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Second Color</label>
                                                                                <input type="text" name="second" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Third Color</label>
                                                                                <input type="text" name="third" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                            </div>
                                                                            <div class="form-group col-md-6">
                                                                                <label for="exampleInputEmail1">Fourth Color</label>
                                                                                <input type="text" name="fourth" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                            </div>
                                                                        </div>

                                                                        <div class="modal-footer" style="margin-top:1rem;">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="submit" class="btn btn-primary">Save
                                                                                changes</button>
                                                                        </div>
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- widget content -->
                                                    <div class="widget-body p-0">


                                                        <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                            <thead>
                                                            <tr>
                                                                <th data-hide="phone">ID</th>
                                                                <th data-class="expand">First Color</th>
                                                                <th data-class="expand">Second Color</th>

                                                                <th data-class="expand">Third Color</th>
                                                                <th data-class="expand">Fourth Color</th>

                                                                <th data-hide="phone,tablet">Action</th>

                                                            </tr>
                                                            </thead>
                                                            <tbody>


                                                            @foreach($mobile_colors as $mobile_color)

                                                                <tr>

                                                                    <td>{{$mobile_color->id}}</td>
                                                                    <td>{{$mobile_color->first}}</td>
                                                                    <td>{{$mobile_color->second}}</td>
                                                                    <td>{{$mobile_color->third}}</td>
                                                                    <td>{{$mobile_color->fourth}}</td>

                                                                    <td>
                                                                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job-mob{{$mobile_color->id}}" style="margin-bottom:10px;"><i
                                                                                    class="fa fa-edit"></i></button>
                                                                        <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob-mob{{$mobile_color->id}}" style="margin-bottom:10px;"><i
                                                                                    class="fa fa-trash"></i></button>

                                                                    </td>

                                                                </tr>

                                                            @endforeach

                                                            @foreach($mobile_colors as $mobile_color)


                                                            <!-- Modal  to Edit image-->
                                                            <div class="modal fade" id="myModal-job-mob{{$mobile_color->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                                Edit Color</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            <form action="{{route('admin.settings.edit.mobile.color',['id'=>$mobile_color->id])}}"
                                                                                  method="post" enctype="multipart/form-data">
                                                                                @csrf
                                                                                <div class="row">
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">First Color</label>
                                                                                        <input type="text" name="first" value="{{$mobile_color->first}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Second Color</label>
                                                                                        <input type="text" name="second" value="{{$mobile_color->second}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Third Color</label>
                                                                                        <input type="text" name="third" value="{{$mobile_color->third}}" class="form-control" id="exampleInputText1" placeholder="#ffffff">
                                                                                    </div>
                                                                                    <div class="form-group col-md-6">
                                                                                        <label for="exampleInputEmail1">Fourth Color</label>
                                                                                        <input type="text" name="fourth" value="{{$mobile_color->fourth}}" class="form-control" id="exampleInputText1" placeholder="#000000">
                                                                                    </div>
                                                                                </div>

                                                                                <div class="modal-footer" style="margin-top:1rem;">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                    <button type="submit" class="btn btn-primary">Save
                                                                                        changes</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>



                                                            <!-- Modal to delet image -->
                                                            <div class="modal fade" id="myModal-deljob-mob{{$mobile_color->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                                 aria-hidden="true">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            </h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <form action="{{route('admin.settings.delete.mobile.color',['id'=>$web_color->id])}}"
                                                                              method="post">
                                                                            @csrf
                                                                        <div class="modal-body" style="text-align:left;">
                                                                            Are you sure you want to
                                                                            <strong>Delet This
                                                                                Color </strong> ?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                            <button type="submit" class="btn btn-primary">Yes</button>
                                                                        </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @endforeach

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                    <!-- end widget content -->

                                                </div>
                                                <!-- end widget div -->

                                            </div>
                                            <!-- end widget -->
                                        </div>
                                        <div class="tab-pane" id="Web" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-1-web"> Add Slider Image</a>

                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-1-web" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Image</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form action="{{route('admin.settings.add.web.slider')}}"
                                                                      method="post">
                                                                    @csrf
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> Name In Arabic</label>
                                                                            <input type="text" name="name_ar" class="form-control" id="exampleInputText1" placeholder="Image Name In Arabic">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1"> Name In English</label>
                                                                            <input type="text" name="name_en" class="form-control" id="exampleInputText1" placeholder="Image Name In English">
                                                                        </div>


                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="img" id="image9-slid" accept=".gif, .jpg, .png" />
                                                                            <label for="image9-slid">
                                                                                <span>image</span>
                                                                            </label>
                                                                        </div>


                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="submit" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Image Name In Arabic</th>
                                                            <th data-class="expand">Image Name In English</th>

                                                            <th data-hide="phone">Image</th>
                                                            <th data-hide="phone">Condition</th>

                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($web_sliders as $web_slider)


                                                            <tr>

                                                            <td>{{$web_slider->id}}</td>
                                                            <td>{{$web_slider->name_ar}}</td>
                                                            <td>{{$web_slider->name_en}}</td>



                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="{{asset('uploads/sliders/' . $web_slider->img)}}" data-img="{{asset('uploads/sliders/' . $web_slider->img)}}"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <div class="form-group">
                                                                    <select class="form-control status"
                                                                            uid="{{ $web_slider->id }}">
                                                                        <option @if($web_slider->is_active) selected
                                                                                @endif value="1">
                                                                            {{trans('admin.active')}}
                                                                        </option>
                                                                        <option @if(!$web_slider->is_active) selected
                                                                                @endif value="0">
                                                                            {{trans('admin.pending')}}
                                                                        </option>
                                                                    </select>

                                                                </div>
                                                            </td>

                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job{{$web_slider->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob{{$web_slider->id}}" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>

                                                            @endforeach

                                                        @foreach($web_sliders as $web_slider)

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-job{{$web_slider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Edit Details</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form action="{{route('admin.settings.edit.web.slider',['id'=>$web_slider->id])}}"
                                                                              method="post" enctype="multipart/form-data">
                                                                            @csrf
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In Arabic</label>
                                                                                    <input type="text" name="name_ar" value="{{$web_slider->name_ar}}" class="form-control" id="exampleInputText1" placeholder="Image Name In Arabic">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Name In English</label>
                                                                                    <input type="text" name="name_en" value="{{$web_slider->name_en}}" class="form-control" id="exampleInputText1" placeholder="Image Name In English">
                                                                                </div>

                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="img" id="image9-web" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9-web" class="file-ok"
                                                                                           style="background-image: url({{url('uploads/sliders/'.$web_slider->img)}});">
                                                                                        <span>image</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="submit" class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-deljob{{$web_slider->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <form action="{{route('admin.settings.delete.web.slider',['id'=>$web_slider->id])}}"
                                                                          method="post">
                                                                        @csrf
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Worker </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="submit" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    @endforeach
                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                            <!-- end widget div -->
                                        </div>
                                        <div class="tab-pane" id="Mobile" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-mob"> Add Slider Image</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-mob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Image</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form>
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Image Name In Arabic</label>
                                                                            <input type="text" class="form-control" id="exampleInputText1" placeholder="Image Name In Arabic">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Image Name In English</label>
                                                                            <input type="text" class="form-control" id="exampleInputText1" placeholder="Image Name In English">
                                                                        </div>



                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="image9" id="image9" accept=".gif, .jpg, .png" />
                                                                            <label for="image9">
                                                                                <span>image</span>
                                                                            </label>
                                                                        </div>


                                                                    </div>

                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Image Name In Arabic</th>
                                                            <th data-class="expand">Image Name In English</th>

                                                            <th data-hide="phone">Image</th>
                                                            <th data-hide="phone">Condition</th>

                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        <tr>

                                                            <td>Lee</td>
                                                            <td>Lee</td>
                                                            <td>Lee</td>



                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="assets/img/superbox/superbox-thumb-1.jpg" data-img="assets/img/superbox/superbox-full-1.jpg"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>

                                                            <td>
                                                                <select>
                                                                    <option>active</option>
                                                                    <option>suspended</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-job" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-deljob" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>



                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-job" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Edit Details</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form>
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Image Name In Arabic</label>
                                                                                    <input type="text" class="form-control" id="exampleInputText1" placeholder="Image Name In Arabic">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Image Name In English</label>
                                                                                    <input type="text" class="form-control" id="exampleInputText1" placeholder="Image Name In English">
                                                                                </div>



                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="image9" id="image9" accept=".gif, .jpg, .png" />
                                                                                    <label for="image9">
                                                                                        <span>image</span>
                                                                                    </label>
                                                                                </div>


                                                                            </div>

                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="button" class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-deljob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                        </h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Worker </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="button" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="Testimonial" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-tgalls"> Add Testimonial</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-tgalls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Customer Logo</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form>
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Customer Name</label>
                                                                            <input type="text" class="form-control" id="exampleInputText1" placeholder="Name">
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Customer Say</label>
                                                                            <input type="text" class="form-control" id="exampleInputText1" placeholder="Say">
                                                                        </div>
                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="image6" id="image6" accept=".gif, .jpg, .png" />
                                                                            <label for="image6">
                                                                                <span>Uplaod Image</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Customer Name</th>
                                                            <th data-class="expand">Customer Say</th>
                                                            <th data-hide="phone">Image</th>
                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        <tr>

                                                            <td>15</td>
                                                            <td>Panda</td>
                                                            <td>
                                                                <a class="btn btn-info" data-toggle="modal" data-target="#myModal-tgall5" style="margin-top:10px;color:#fff">Show
                                                                    Details</button>
                                                            </td>
                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="assets/img/superbox/superbox-thumb-1.jpg" data-img="assets/img/superbox/superbox-full-1.jpg"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-tgall3" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-tgall4" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-tgall5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> this is my text image details</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-tgall2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> this is my text image details</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-tgall3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            Testmonial</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form>
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Customer Name</label>
                                                                                    <input type="text" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                                </div>
                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1"> Jop Title</label>
                                                                                    <input type="text" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                                </div>
                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="image6" id="image6" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6">
                                                                                        <span>Uplaod Image</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="button" class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-tgall4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Image </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="button" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                        <div class="tab-pane" id="clogo" role="tabpanel">
                                            <div style="text-align: center;">
                                                <a style=" margin-top: 1rem;color:#fff;margin-bottom:1rem;" href="javascript:void(0);" class="btn sa-btn-primary" data-toggle="modal"
                                                   data-target="#myModal-cgalls"> Add Image</a>



                                                <!-- Modal  to add image-->
                                                <div class="modal fade" id="myModal-cgalls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                     aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                    Customer Logo</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body" style="text-align:left;">
                                                                <form>
                                                                    <div class="row">


                                                                        <div class="form-group col-md-6">
                                                                            <label for="exampleInputEmail1">Customer Name</label>
                                                                            <input type="text" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                        </div>
                                                                        <div class="wrap-custom-file ">
                                                                            <input type="file" name="image6" id="image6" accept=".gif, .jpg, .png" />
                                                                            <label for="image6">
                                                                                <span>Uplaod Image</span>
                                                                            </label>
                                                                        </div>

                                                                    </div>


                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-primary">Save
                                                                            changes</button>
                                                                    </div>
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- widget content -->
                                                <div class="widget-body p-0">


                                                    <table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th data-hide="phone">ID</th>
                                                            <th data-class="expand">Customer Name</th>
                                                            <th data-hide="phone">Image</th>
                                                            <th data-hide="phone,tablet">Action</th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        <tr>

                                                            <td>15</td>
                                                            <td>Panda</td>

                                                            <td>
                                                                <div class="superbox-list superbox-8">
                                                                    <img src="assets/img/superbox/superbox-thumb-1.jpg" data-img="assets/img/superbox/superbox-full-1.jpg"
                                                                         alt="" title="" class="superbox-img" style="height: 73px;width: 118px;">
                                                                </div>
                                                            </td>


                                                            <td>
                                                                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal-cgall3" style="margin-bottom:10px;"><i
                                                                            class="fa fa-edit"></i></button>
                                                                <button class="btn btn-danger" data-toggle="modal" data-target="#myModal-cgall4" style="margin-bottom:10px;"><i
                                                                            class="fa fa-trash"></i></button>

                                                            </td>

                                                        </tr>
                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cgall5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> this is my text image details</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to show image-->
                                                        <div class="modal fade" id="myModal-cgall2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">
                                                                            Image Text</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <p> this is my text image details</p>
                                                                    </div>
                                                                    <div class="modal-footer" style="margin-top:1rem;">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- Modal  to Edit image-->
                                                        <div class="modal fade" id="myModal-cgall3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Add
                                                                            User</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        <form>
                                                                            <div class="row">


                                                                                <div class="form-group col-md-6">
                                                                                    <label for="exampleInputEmail1">Customer Name</label>
                                                                                    <input type="text" class="form-control" id="exampleInputText1" placeholder="Name In Arabic">
                                                                                </div>
                                                                                <div class="wrap-custom-file ">
                                                                                    <input type="file" name="image6" id="image6" accept=".gif, .jpg, .png" />
                                                                                    <label for="image6">
                                                                                        <span>Uplaod Image</span>
                                                                                    </label>
                                                                                </div>

                                                                            </div>


                                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                                <button type="button" class="btn btn-primary">Save
                                                                                    changes</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>



                                                        <!-- Modal to delet image -->
                                                        <div class="modal fade" id="myModal-cgall4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                                             aria-hidden="true">
                                                            <div class="modal-dialog" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel" style="font-weight:bold">Delet
                                                                            Image</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body" style="text-align:left;">
                                                                        Are you sure you want to
                                                                        <strong>Delet This
                                                                            Image </strong> ?
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                        <button type="button" class="btn btn-primary">Yes</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        </tbody>
                                                    </table>

                                                </div>
                                                <!-- end widget content -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->
                </div>
            </div>
        </div>

    </div>
@stop

@section('scripts')
    <script>
        $('#tabs').tabs();
        // Dynamic tabs
        var tabTitle = $("#tab_title"), tabContent = $("#tab_content"),
            tabTemplate = "<li style='position:relative;'> <span class='air air-top-left delete-tab' style='top:5px; left:5px;'><button class='btn btn-xs font-xs btn-default hover-transparent'><i class='fa fa-times'></i></button></span></span><a href='#{href}'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; #{label}</a></li>",
            tabCounter = 2;

        var tabs = $("#tabs2").tabs();

        // modal dialog init: custom buttons and a "close" callback reseting the form inside
        var dialog = $("#addtab").dialog({
            autoOpen: false,
            width: 600,
            resizable: false,
            modal: true,
            buttons: [{
                html: "<i class='fa fa-times'></i>&nbsp; Cancel",
                "class": "btn btn-default",
                click: function () {
                    $(this).dialog("close");

                }
            }, {

                html: "<i class='fa fa-plus'></i>&nbsp; Add",
                "class": "btn sa-btn-danger",
                click: function () {
                    addTab();
                    $(this).dialog("close");
                }
            }]
        });
        // addTab form: calls addTab function on submit and closes the dialog
        var form = dialog.find("form").submit(function (event) {
            addTab();
            dialog.dialog("close");
            event.preventDefault();
        });

        // actual addTab function: adds new tab using the input from the form above

    </script>

    <script>
        $(document).on("change", ".status", function () {
            var status = $(this).val();
            var id = $(this).attr("uid");
            var token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('admin.settings.edit.status') }}",
                type: "post",
                dataType: "json",
                data: {status: status, id: id, _token: token},
                success: function (data) {
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: data.message,
                        showConfirmButton: false,
                        width: '40rem',
                        customClass: "right-check",
                        timer: 1500
                    });
                    if (data.status !== "1") {
                        Swal.fire({
                            type: 'error',
                            title: 'حدث خطأ',
                        });
                    }
                },
                error: function () {
                    Swal.fire({
                        type: 'error',
                        title: 'حدث خطأ',
                    });
                }
            })
        })
    </script>

@stop