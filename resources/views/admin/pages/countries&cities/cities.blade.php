@extends('admin.layouts.Master-Layout')

@section('title')

    {{trans('admin.city')}}

@stop

@section('content')
    <div class="sa-content-wrapper">

        <div class="sa-content">
            <div class="d-flex w-100 home-header">
                <div>
                    <h1 class="page-header"><i class="fa fa-table fa-fw "></i> {{trans('admin.dashboard')}} <span>>  {{trans('admin.city')}}
								</span></h1>
                </div>
            </div>
            <div>
                <div>
                    <!-- widget grid -->
                    <section id="widget-grid" class="">
                        <!-- row -->
                        <div class="row">
                            <!-- NEW WIDGET START -->
                            <article class="col-12">
                                <!-- Widget ID (each widget will need unique ID)-->
                                <div class="jarviswidget jarviswidget-color-darken no-padding" id="wid-id-3"
                                     data-widget-editbutton="false">
                                    <header>
                                        <div class="widget-header">
                                            <span class="widget-icon"> <i class="fa fa-lg  fa-globe"></i> </span>
                                        </div>
                                    </header>
                                    <!-- widget div-->
                                    <div style="text-align: center;">
                                        <a style=" margin-top: 1rem;" href="javascript:void(0);"
                                           class="btn sa-btn-primary" data-toggle="modal"
                                               data-target="#myModal-1"> {{trans('admin.add')}}</a>


                                        <!-- Modal  to add supervisor-->
                                        <div class="modal fade" id="myModal-1" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel"
                                                            style="font-weight:bold">{{trans('admin.add')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body" style="text-align:left;">
                                                        <form action="{{route('admin.cities.add.city',['country_id'=>$id])}}"
                                                              method="post">
                                                            @csrf
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.name')}}</label>
                                                                <input type="text" class="form-control"
                                                                       id="exampleInputText1" name="name"
                                                                       value="{{old('name')}}"
                                                                       placeholder="{{trans('admin.name')}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInputEmail1">{{trans('admin.shipping')}}</label>
                                                                <input type="text" class="form-control"
                                                                       id="exampleInputText1" name="shipping"
                                                                       value="{{old('shipping')}}"
                                                                       placeholder="{{trans('admin.shipping')}}">
                                                            </div>
                                                            <div class="modal-footer" style="margin-top:1rem;">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">{{trans('admin.cancel')}}
                                                                </button>
                                                                <button type="submit"
                                                                        class="btn btn-primary">{{trans('admin.confirm')}}
                                                                </button>
                                                            </div>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- widget content -->
                                        <div class="widget-body p-0">
                                            <table id="dt-basic"
                                                   class="table table-striped table-bordered table-hover"
                                                   width="100%">
                                                <thead>
                                                <tr>
                                                    <th data-hide="phone">{{trans('admin.id')}}</th>
                                                    <th data-class="expand">{{trans('admin.country')}}</th>
                                                    <th data-class="expand">{{trans('admin.city')}}</th>
                                                    <th data-class="expand">{{trans('admin.shipping')}}</th>
                                                    <th data-hide="phone,tablet">{{trans('admin.action')}}</th>

                                                </tr>
                                                </thead>
                                                <tbody>

                                                @foreach($cities as $city)
                                                    <tr>
                                                        <td>{{$city->id}}</td>
                                                        <td>{{$city->country->name}}</td>
                                                        <td>{{$city->name}}</td>
                                                        <td>{{$city->shipping}}</td>
                                                        <td>
                                                            <button class="btn btn-primary" data-toggle="modal"
                                                                    data-target="#myModal-2{{$city->id}}"
                                                                    style="margin-bottom:10px;"><i
                                                                        class="fa fa-edit"></i></button>
                                                            <button class="btn btn-danger" data-toggle="modal"
                                                                    data-target="#myModal-3{{$city->id}}"
                                                                    style="margin-bottom:10px;"><i
                                                                        class="fa fa-trash"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                        <!-- end widget content -->

                                    </div>
                                    <!-- end widget div -->

                                </div>
                                <!-- end widget -->

                            </article>
                            <!-- WIDGET END -->

                        </div>

                        <!-- end row -->

                        <!-- end row -->

                    </section>
                    <!-- end widget grid -->

                </div>
            </div>
        </div>
    </div>
    @foreach($cities as $city)

        <!-- Modal  to Edit supervisor-->
        <div class="modal fade" id="myModal-2{{$city->id}}" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel"
                            style="font-weight:bold">{{trans('admin.edit')}}</h5>
                        <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"
                         style="text-align:left;">
                        <form action="{{route('admin.cities.edit.city',['city_id'=>$city->id])}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label
                                        for="exampleInputEmail1">{{trans('admin.name')}}</label>
                                <input type="text"
                                       class="form-control"
                                       id="exampleInputText1"
                                       placeholder="{{trans('admin.name')}}" name="name" value="{{$city->name}}">
                            </div>
                            <div class="form-group">
                                <label
                                        for="exampleInputEmail1">{{trans('admin.shipping')}}
                                    </label>
                                <input type="text"
                                       class="form-control"
                                       id="exampleInputText1"
                                       placeholder="{{trans('admin.shipping')}}" name="shipping" value="{{$city->shipping}}">
                            </div>
                            <div class="modal-footer"
                                 style="margin-top:1rem;">
                                <button type="button"
                                        class="btn btn-default"
                                        data-dismiss="modal">{{trans('admin.cancel')}}
                                </button>
                                <button type="submit"
                                        class="btn btn-primary">{{trans('admin.confirm')}}
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <!-- Modal to delet supervisor -->
        <div class="modal fade" id="myModal-3{{$city->id}}" tabindex="-1"
             role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"
                            id="exampleModalLabel"
                            style="font-weight:bold">{{trans('admin.delete')}}</h5>
                        <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body"
                         style="text-align:left;">
                     {{trans('admin.delete.confirm')}}
                    </div>
                    <form action="{{route('admin.cities.delete.city',['city_id'=>$city->id])}}"  method="post">
                        @csrf
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-default"
                                    data-dismiss="modal">{{trans('admin.cancel')}}
                            </button>
                            <button type="submit"
                                    class="btn btn-primary">{{trans('admin.confirm')}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@stop