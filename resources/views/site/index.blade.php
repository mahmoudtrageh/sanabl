@extends('layouts.Site-Layout')


@section('title')

    <title> Sanabl | Home</title>

@stop

@section('content')

    <!-- Start slider  -->
    <section id="mu-slider">
        <div class="mu-slider-area">
            <!-- Top slider -->
            <div class="mu-top-slider">
                <!-- Top slider single slide -->
                <div class="mu-top-slider-single">
                    <img src="assets/img/slider/uptodate.jpg" alt="img">
                    <!-- Top slider content -->
                    <div class="mu-top-slider-content">
                        <span class="mu-slider-small-title">اهلا بكم</span>
                        <!-- <h2 class="mu-slider-title">TO THE SPICYX</h2> -->
                        <p>نؤمن بأهمية الوقت , ولدينا القدرة على التسويق بدقة وإتقان وخبرة</p>
                        <!-- <a href="#" class="mu-readmore-btn">READ MORE</a> -->
                    </div>
                    <!-- / Top slider content -->
                </div>
                <!-- / Top slider single slide -->
                <!-- Top slider single slide -->
                <div class="mu-top-slider-single">
                    <img src="assets/img/slider/collaboration-illustration.jpg" alt="img">
                    <!-- Top slider content -->
                    <div class="mu-top-slider-content">
                        <span class="mu-slider-small-title">المصداقية</span>
                        <!-- <h2 class="mu-slider-title">GREEN FOODS</h2> -->
                        <p>التجدد سمة عروضنا , والمصداقية مع عميلنا سبب نجاحنا وتواصلنا</p>
                        <!-- <a href="#" class="mu-readmore-btn">READ MORE</a> -->
                    </div>
                    <!-- / Top slider content -->
                </div>
                <!-- / Top slider single slide -->
                <!-- Top slider single slide -->
                <div class="mu-top-slider-single">
                    <img src="assets/img/slider/blog_header-2.png" alt="img">
                    <!-- Top slider content -->
                    <div class="mu-top-slider-content">
                        <span class="mu-slider-small-title">النظام</span>
                        <!-- <h2 class="mu-slider-title">SPICY MASALAS</h2> -->
                        <p>شعارنا النظام ومراقبة الله فى العمل</p>
                        <!-- <a href="#" class="mu-readmore-btn">READ MORE</a> -->
                    </div>
                    <!-- / Top slider content -->
                </div>
                <!-- / Top slider single slide -->
            </div>
        </div>
    </section>
    <!-- End slider  -->
    <!-- start welcome -->
    <!-- <section id="mu-welcome" dir="rtl">
    <div class="container">
        <div class="row">
          <div class="mu-welcome-area">
          <div class="mu-title">
            <h2>اهلا بكم</h2>
            <i class="fa fa-globe" aria-hidden="true"></i>
            <span class="mu-title-bar"></span>
          </div>
          <marquee behavior="alternate" style="margin-top:50px;">
            <div class="col-lg-2 col-xs-2 text-center">
              <div class="hello">
                <div class="spicy">
                    <img src="assets/img/pumpkin.png">
                    </div>
                    <h5>المنتجات الزراعية</h5>
                    </div>
            </div>
            <div class="col-lg-2 col-xs-2 text-center">
              <div class="hello">
               <div class="spicy">
                    <img src="assets/img/milk-products.png">
                    </div>
                    <h5>منتجات يومية طازجة</h5>
                    </div>
              </div>
              <div class="col-lg-2 col-xs-2 text-center">
                <div class="hello">
                 <div class="spicy">
                      <img src="assets/img/sunrise.png">
                      </div>
                      <h5>ابتكار الحصاد</h5>
                      </div>
                </div>
                <div class="col-lg-2 col-xs-2 text-center">
                  <div class="hello">
                   <div class="spicy">
                        <img src="assets/img/cow.png">
                        </div>
                        <h5>منتجات اللحوم</h5>
                        </div>
                  </div>
                  <div class="col-lg-2 col-xs-2 text-center">
                    <div class="hello">
                     <div class="spicy">
                          <img src="assets/img/chicken.png">
                          </div>
                          <h5>منتجات الدواجن</h5>
                          </div>
                    </div>
                    <div class="col-lg-2 col-xs-2 text-center">
                      <div class="hello">
                        <div class="spicy">
                            <img src="assets/img/seeds.png">
                            </div>
                            <h5>الاسمدة العضوية</h5>
                            </div>
                      </div>
    </marquee>
          </div>
          </div>
       </div>

    </section> -->
    <!-- end welcome -->

    <section>
        <div class="mu-welcome-area">
            <div class="mu-title">
                <h2>اهلا بكم</h2>
                <i class="fa fa-globe" aria-hidden="true"></i>
                <span class="mu-title-bar"></span>
            </div>
            <marquee  behavior="alternate" style="margin-top:50px;">
                <div style="display: inline-block"><div class="hello">
                        <div class="spicy">
                            <img src="assets/img/seeds.png">
                        </div>
                        <h5>الاسمدة العضوية</h5>
                    </div> </div>
                <div style="display: inline-block"> <div class="hello">
                        <div class="spicy">
                            <img src="assets/img/pumpkin.png">
                        </div>
                        <h5>المنتجات الزراعية</h5>
                    </div></div>
                <div style="display: inline-block"> <div class="hello">
                        <div class="spicy">
                            <img src="assets/img/chicken.png">
                        </div>
                        <h5>منتجات الدواجن</h5>
                    </div></div>
                <div style="display: inline-block"> <div class="hello">
                        <div class="spicy">
                            <img src="assets/img/cow.png">
                        </div>
                        <h5 style="margin-left:8px;">منتجات اللحوم</h5>
                    </div></div>
                <div style="display: inline-block">  <div class="hello">
                        <div class="spicy">
                            <img src="assets/img/sunrise.png">
                        </div>
                        <h5 style="margin-left:8px;">ابتكار الحصاد</h5>
                    </div></div>
                <div style="display: inline-block">  <div class="hello">
                        <div class="spicy">
                            <img src="assets/img/milk-products.png">
                        </div>
                        <h5>منتجات يومية طازجة</h5>
                    </div></div>
                <!-- <div style="display: inline-block">gogogogogogog</div>
                <div style="display: inline-block">gogogogogogog</div>
                <div style="display: inline-block">gogogogogogog</div>
                <div style="display: inline-block">gogogogogogog</div> -->

            </marquee>
        </div>
    </section>


    <!-- Start About us -->
    <section id="mu-about-us">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-about-us-area">
                        <div class="mu-title">
                            <!-- <span class="mu-subtitle">شركتنا</span> -->
                            <h2>من نحن </h2>
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <span class="mu-title-bar"></span>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mu-about-us-left text-right">
                                    <p>في بعض الأوقات تكون اعمال الدعاية والاعلان كبيره وتحتاج لمجهود ومتابعه من قبل أصحاب الاعمال الصغيرة , ولذلك تقوم شركات الدعاية والاعلان بهذا الدور نيابة عن اصحاب الاعمال وتقدم لها مجموعة من الخدمات تساعدها في تطوير نشاطها الاعلاني وتجعل كامل تركيز اصحاب الاعمال علي تشغيل الشركة والتطوير منها , ولذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية والاعلان .</p>
                                    <ul class="text-right">
                                        <li>ولذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية وا</li>
                                        <li>لذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية</li>
                                        <li>لذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية </li>
                                        <li>لذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية</li>
                                        <li>لذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية</li>
                                        <li>لذلك في نقاط سنعرض أهم الخدمات التي تقدمها شركات الدعاية </li>
                                    </ul>
                                    <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque similique molestias est quod reprehenderit, quibusdam nam qui, quam magnam. Ex.</p>   -->
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mu-about-us-right">
                                    <ul class="mu-abtus-slider">
                                        <li><img src="assets/img/about-us/9d8a53d8ee2918953439588d3054.png" alt="img"></li>
                                        <li><img src="assets/img/about-us/500_F_199490348_8tfIi1fdtkoIsFbH3SQygFPqHLOvOzbM.jpg" alt="img"></li>
                                        <li><img src="assets/img/about-us/500_F_199490348_8tfIi1fdtkoIsFbH3SQygFPqHLOvOzbM.jpg" alt="img"></li>
                                        <li><img src="assets/img/about-us/Marketing-Consultancy-min.jpg" alt="img"></li>
                                        <li><img src="assets/img/about-us/500_F_199490348_8tfIi1fdtkoIsFbH3SQygFPqHLOvOzbM.jpg" alt="img"></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End About us -->

    <!-- Start Counter Section -->
    <section id="mu-counter">
        <div class="mu-counter-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mu-counter-area">
                            <ul class="mu-counter-nav">
                                <li class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="mu-single-counter">
                                        <span>Fresh</span>
                                        <h3><span class="counter">55</span><sup>+</sup></h3>
                                        <p>Breakfast Items</p>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="mu-single-counter">
                                        <span>Delicious</span>
                                        <h3><span class="counter">130</span><sup>+</sup></h3>
                                        <p>Lunch Items</p>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="mu-single-counter">
                                        <span>Hot</span>
                                        <h3><span class="counter">35</span><sup>+</sup></h3>
                                        <p>Coffee Items</p>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="mu-single-counter">
                                        <span>Satisfied</span>
                                        <h3><span class="counter">3562</span><sup>+</sup></h3>
                                        <p>Customers</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Counter Section -->

    <!-- Start Restaurant Menu -->
    <!-- <section id="mu-restaurant-menu">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="mu-restaurant-menu-area">
              <div class="mu-title"> -->
    <!-- <span class="mu-subtitle">Discover</span> -->
    <!-- <h2>منتاجتنا</h2>
    <i class="fa fa-globe" aria-hidden="true"></i>
    <span class="mu-title-bar"></span>
  </div>
  <div class="mu-restaurant-menu-content">
    <ul class="nav nav-tabs mu-restaurant-menu">
      <li class="active"><a href="#breakfast" data-toggle="tab">تسويق الكترونى</a></li>
      <li><a href="#meals" data-toggle="tab">تصميم مواقع</a></li>
      <li><a href="#snacks" data-toggle="tab">ليزر حفر </a></li>
      <li><a href="#desserts" data-toggle="tab">مجسمات فوم</a></li>
      <li><a href="#drinks" data-toggle="tab">طباعة </a></li>
    </ul> -->

    <!-- Tab panes -->
    <!-- <div class="tab-content">
      <div class="tab-pane fade in active" id="breakfast">
        <div class="mu-tab-content-area">
          <div class="row">
            <div class="col-md-6">
              <div class="mu-tab-content-left">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-1.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-2.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-1.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
           <div class="col-md-6">
             <div class="mu-tab-content-right">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-1.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-2.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-1.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
             </div>
           </div>
         </div>
       </div>
      </div>
      <div class="tab-pane fade " id="meals">
        <div class="mu-tab-content-area">
          <div class="row">
            <div class="col-md-6">
              <div class="mu-tab-content-left">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-3.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-4.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-3.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
           <div class="col-md-6">
             <div class="mu-tab-content-right">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-4.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-3.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-4.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
             </div>
           </div>
         </div>
       </div>
      </div>
      <div class="tab-pane fade " id="snacks">
        <div class="mu-tab-content-area">
          <div class="row">
            <div class="col-md-6">
              <div class="mu-tab-content-left">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-5.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-6.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-5.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
           <div class="col-md-6">
             <div class="mu-tab-content-right">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-5.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-6.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-5.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
             </div>
           </div>
         </div>
       </div>
      </div>
      <div class="tab-pane fade " id="desserts">
        <div class="mu-tab-content-area">
          <div class="row">
            <div class="col-md-6">
              <div class="mu-tab-content-left">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-7.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-8.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-7.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
           <div class="col-md-6">
             <div class="mu-tab-content-right">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-8.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-7.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-8.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
             </div>
           </div>
         </div>
       </div>
      </div>
      <div class="tab-pane fade " id="drinks">
        <div class="mu-tab-content-area">
          <div class="row">
            <div class="col-md-6">
              <div class="mu-tab-content-left">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-9.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-10.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-9.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
           <div class="col-md-6">
             <div class="mu-tab-content-right">
                <ul class="mu-menu-item-nav">
                  <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-9.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">English Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-10.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Chines Breakfast</a></h4>
                        <span class="mu-menu-price">$11.95</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                   <li>
                    <div class="media">
                      <div class="media-left">
                        <a href="#">
                          <img class="media-object" src="assets/img/menu/item-9.jpg" alt="img">
                        </a>
                      </div>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="#">Indian Breakfast</a></h4>
                        <span class="mu-menu-price">$15.85</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere nulla aliquid praesentium dolorem commodi illo.</p>
                      </div>
                    </div>
                  </li>
                </ul>
             </div>
           </div>
         </div>
       </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section> -->
    <!-- End Restaurant Menu -->

    <!-- Start Reservation section -->
    <!-- <section id="mu-reservation">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="mu-reservation-area">
              <div class="mu-title">
                <span class="mu-subtitle">Make A</span>
                <h2>Reservation</h2>
                <i class="fa fa-spoon"></i>
                <span class="mu-title-bar"></span>
              </div>
              <div class="mu-reservation-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ratione quidem autem iusto, perspiciatis, amet, quaerat blanditiis ducimus eius recusandae nisi aut totam alias consectetur et.</p>
                <form class="mu-reservation-form">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Full Name">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Phone Number">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <select class="form-control">
                          <option value="0">How Many?</option>
                          <option value="1 Person">1 Person</option>
                          <option value="2 People">2 People</option>
                          <option value="3 People">3 People</option>
                          <option value="4 People">4 People</option>
                          <option value="5 People">5 People</option>
                          <option value="6 People">6 People</option>
                          <option value="7 People">7 People</option>
                          <option value="8 People">8 People</option>
                          <option value="9 People">9 People</option>
                          <option value="10 People">10 People</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" id="datepicker" placeholder="Date">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Phone No">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <textarea class="form-control" cols="30" rows="10" placeholder="Your Message"></textarea>
                      </div>
                    </div>
                    <button type="submit" class="mu-readmore-btn">Make Reservation</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>   -->
    <!-- End Reservation section -->
    <!-- Start Chef Section -->
    <section id="mu-chef">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-chef-area">
                        <div class="mu-title">
                            <!-- <span class="mu-subtitle">Our Professionals</span> -->
                            <h2>خدماتنا</h2>
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <span class="mu-title-bar"></span>
                        </div>
                        <div class="mu-chef-content">
                            <ul class="mu-chef-nav">
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/web designing.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>تصميم مواقع</h4>

                                            <!-- <span>Head Chef</span> -->
                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/foam.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>مجسمات فوم</h4>

                                            <!-- <span>Pizza Chef</span> -->
                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/resume_solutions_image.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>طباعة ان دور </h4>

                                            <!-- <span>Grill Chef</span> -->
                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/web designing.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>Marty Fukuda</h4>

                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/foam.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>Simon Jonson</h4>

                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/resume_solutions_image.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>Kelly Wenzel</h4>

                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/foam.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>Greg Hong</h4>

                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="mu-single-chef">
                                        <figure class="mu-single-chef-img">
                                            <img src="assets/img/chef/web designing.jpg" alt="chef img">
                                        </figure>
                                        <div class="mu-single-chef-info">
                                            <h4>Marty Fukuda</h4>

                                        </div>
                                        <div class="mu-single-chef-social">
                                            <!-- <a href="#"><i class="fa fa-facebook"></i></a>
                                            <a href="#"><i class="fa fa-twitter"></i></a>
                                            <a href="#"><i class="fa fa-google-plus"></i></a>
                                            <a href="#"><i class="fa fa-linkedin"></i></a> -->
                                            <p style="color:wheat;">بننىعلىلتب</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Chef Section -->
    <!-- Start Gallery -->
    <section id="mu-gallery">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-gallery-area">
                        <div class="mu-title">
                            <!-- <span class="mu-subtitle">Discover</span> -->
                            <h2>منتاجتنا</h2>
                            <i class="fa fa-globe" aria-hidden="true"></i>


                            <span class="mu-title-bar"></span>
                        </div>
                        <div class="mu-gallery-content">
                            <div class="mu-gallery-top">
                                <!-- Start gallery menu -->
                                <ul>

                                    <li class="filter" data-filter=".food">طباعة</li>
                                    <li class="filter" data-filter=".drink">تصميم المواقع</li>
                                    <li class="filter" data-filter=".restaurant">طباعة اوفسيت و ديجيتال</li>
                                    <li class="filter" data-filter=".dinner">مجسمات فوم</li>
                                    <li class="filter" data-filter=".dessert">تسويق الكترونى</li>
                                    <li class="filter active" data-filter="all">الكل</li>
                                </ul>
                            </div>
                            <!-- Start gallery image -->
                            <div class="mu-gallery-body" id="mixit-container">
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix food">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/1.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/1.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix drink">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/2.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/2.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix restaurant">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/3.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/3.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix dinner">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/4.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/4.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix dinner">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/5.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/5.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix food">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/6.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/6.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix drink">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/7.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/7.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix restaurant">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/8.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/8.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                                <!-- start single gallery image -->
                                <div class="mu-single-gallery col-md-4 mix dessert">
                                    <div class="mu-single-gallery-item">
                                        <figure class="mu-single-gallery-img">
                                            <a href="#"><img alt="img" src="assets/img/gallery/small/9.jpg"></a>
                                        </figure>
                                        <div class="mu-single-gallery-info">
                                            <a href="assets/img/gallery/big/9.jpg" data-fancybox-group="gallery" class="fancybox">
                                                <img src="assets/img/plus.png" alt="plus icon img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- End single gallery image -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Gallery -->



    <!-- Start Subscription section -->
    <!-- <section id="mu-subscription">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="mu-subscription-area">
              <form class="mu-subscription-form">
                <input type="text" placeholder="Type Your Email Address">
                <button class="mu-readmore-btn" type="Submit">SUBSCRIBE</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section> -->
    <!-- End Subscription section -->


    <!-- Start Client Testimonial section -->
    <section id="mu-client-testimonial">
        <div class="mu-overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mu-client-testimonial-area">
                            <div class="mu-title">
                                <!-- <span class="mu-subtitle">Testimonials</span> -->
                                <h2>قالوا عنا</h2>
                                <i class="fa fa-globe" aria-hidden="true"></i>
                                <span class="mu-title-bar"></span>
                            </div>
                            <!-- testimonial content -->
                            <div class="mu-testimonial-content">
                                <!-- testimonial slider -->
                                <ul class="mu-testimonial-slider">
                                    <li>
                                        <div class="mu-testimonial-single text-right">
                                            <div class="mu-testimonial-info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate consequuntur ducimus cumque iure modi nesciunt recusandae eligendi vitae voluptatibus, voluptatum tempore, ipsum nisi perspiciatis. Rerum nesciunt fuga ab natus, dolorem?</p>
                                            </div>
                                            <div class="mu-testimonial-bio">
                                                <p>- David Muller</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mu-testimonial-single">
                                            <div class="mu-testimonial-info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate consequuntur ducimus cumque iure modi nesciunt recusandae eligendi vitae voluptatibus, voluptatum tempore, ipsum nisi perspiciatis. Rerum nesciunt fuga ab natus, dolorem?</p>
                                            </div>
                                            <div class="mu-testimonial-bio">
                                                <p>- David Muller</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="mu-testimonial-single">
                                            <div class="mu-testimonial-info">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate consequuntur ducimus cumque iure modi nesciunt recusandae eligendi vitae voluptatibus, voluptatum tempore, ipsum nisi perspiciatis. Rerum nesciunt fuga ab natus, dolorem?</p>
                                            </div>
                                            <div class="mu-testimonial-bio">
                                                <p>- David Muller</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Client Testimonial section -->


    <!-- Start Contact section -->
    <section id="mu-contact" dir="rtl">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="mu-contact-area">
                        <div class="mu-title">
                            <!-- <span class="mu-subtitle">Get In Touch</span> -->
                            <h2>تواصل معنا</h2>
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <span class="mu-title-bar"></span>
                        </div>
                        <div class="mu-contact-content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mu-contact-left">
                                        <form class="mu-contact-form">
                                            <div class="form-group">
                                                <label for="name">الاسم</label>
                                                <input type="text" class="form-control" id="name" placeholder="الاسم">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">البريد الالكترونى</label>
                                                <input type="email" class="form-control" id="email" placeholder="البريد الالكترونى">
                                            </div>
                                            <div class="form-group">
                                                <label for="Phone">رقم الهاتف</label>
                                                <input type="phone" class="form-control" id="Phone" placeholder="رقم الهاتف">
                                            </div>
                                            <div class="form-group">
                                                <label for="message">الرسالة</label>
                                                <textarea class="form-control" id="message" cols="30" rows="10" placeholder="اكتب رسالتك"></textarea>
                                            </div>
                                            <button type="submit" class="mu-send-btn">ارسل</button>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="mu-contact-right">
                                        <div class="mu-contact-widget">
                                            <h3>تفاصيل التواصل</h3>
                                            <!-- <p>مدينة العبور</p> -->
                                            <address>
                                                <p><i class="fa fa-phone"></i> (850) 457 6688</p>
                                                <p><i class="fa fa-envelope-o"></i>contact@markups.io</p>
                                                <p><i class="fa fa-map-marker"></i>مدينة العبور</p>
                                            </address>
                                        </div>
                                        <div class="mu-contact-widget">
                                            <h3>ساعات العمل</h3>
                                            <address>
                                                <p><span> الجمعة - الاثنين</span> 9.00 am to 12 pm</p>
                                                <p><span>السبت</span> 9.00 am to 10 pm</p>
                                                <p><span>الاحد</span> 10.00 am to 12 pm</p>
                                            </address>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact section -->

    <!-- Start Map section -->
    <section id="mu-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9207.358598888495!2d-85.64847801496286!3d30.183918972289003!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x2320479d70eb6202!2sDillard&#39;s!5e0!3m2!1sbn!2sbd!4v1462359735720" width="100%" height="100%" frameborder="0"allowfullscreen></iframe>
    </section>
    <!-- End Map section -->


@stop

@section('js')



@stop