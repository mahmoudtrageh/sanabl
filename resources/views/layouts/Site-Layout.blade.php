<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>سنابل  </title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('site/img/favicon.ico')}}" type="image/x-icon">

    <!-- Font awesome -->
    <link href="https://fonts.googleapis.com/css?family=El+Messiri" rel="stylesheet">

    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Tangerine' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Prata' rel='stylesheet' type='text/css'>

<head>
    <meta charset="utf-8">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

@yield('title')

@include('assets.Site-Css')

</head>

<body>

<!-- Pre Loader -->
<!-- <div id="aa-preloader-area">
  <div class="mu-preloader">
    <img src="assets/img/preloader.gif" alt=" loader img">
  </div>
</div> -->
<!--START SCROLL TOP BUTTON -->
<a class="scrollToTop" href="#">
    <i class="fa fa-angle-up"></i>
    <span>Top</span>
</a>
<!-- END SCROLL TOP BUTTON -->

<!--start whatsapp icon-->

<a class="whatsapp" href="https://web.whatsapp.com/">
    <i class="fa fa-whatsapp" aria-hidden="true"></i>
</a>

<!--end whatsapp icon-->
<!-- Start header section -->
<header id="mu-header">

    <nav class="navbar navbar-default mu-main-navbar" role="navigation">
        <div class="container">

            <div class="navbar-header">
                <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- LOGO -->
                <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="index.html"><img src="assets/img/logo.png" alt="Logo img"></a>


            </div>
            <div class="row">

                <div class="col-lg-5 col-md-5 col-sm-5">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul id="top-menu" class="nav navbar-nav navbar mu-main-nav">
                            <li class="hidden-lg hidden-md hidden-sm"><a href="#mu-slider">الرئيسية</a></li>
                            <li class="hidden-lg hidden-md hidden-sm"><a href="#mu-about-us">من نحن</a></li>
                            <li class="hidden-lg hidden-md hidden-sm"><a href="#mu-chef">خدماتنا</a></li>

                            <li><a href="#mu-contact">تواصل معنا</a></li>
                            <li><a href="#mu-client-testimonial">قالوا عنا</a></li>
                            <li><a href="#mu-gallery">منتجاتنا</a></li>


                        </ul>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 hidden-xs" >
                    <a class="brand" href="index.html"><img src="assets/img/logo.png" alt="Logo img"></a>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 hidden-xs">
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul id="top-menu" class="nav navbar-nav navbar-right mu-main-nav">
                            <li><a href="#mu-chef"> خدماتنا</a></li>
                            <li><a href="#mu-about-us">من نحن</a></li>
                            <li><a href="#mu-slider">الرئيسية</a></li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->
            </div>
        </div>

    </nav>

</header>
<!-- End header section -->

@yield('content')



<!-- Start Footer -->
<footer id="mu-footer">
    <div class="container">
        <div class="row">
            <div class="mu-footer-area">
                <div class="col-md-6">

                    <div class="mu-footer-social">
                        <a href="#"><span class="fa fa-facebook"></span></a>
                        <a href="#"><span class="fa fa-twitter"></span></a>
                        <a href="#"><span class="fa fa-google-plus"></span></a>
                        <a href="#"><span class="fa fa-linkedin"></span></a>
                        <a href="#"><span class="fa fa-youtube"></span></a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mu-footer-copyright">
                        <p>Designed by <a rel="nofollow" href="http://www.markups.io/">MarkUps.io</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->


@include('assets.Site-JS')
@yield('js')

</body>


<!-- Mirrored from webdesign-finder.com/html/fixetics/index-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 19 Mar 2019 22:21:59 GMT -->
</html>