<?php
/**
 * Created by PhpStorm.
 * User: iki
 * Date: 3/19/19
 * Time: 1:20 PM
 */

return [
    'lang.ar' => 'Arabic',
    'lang.en' => 'English',
    'dashboard' => 'Dashboard',
    'index' => 'Index',
    'admins' => 'Admins',
    'add' => 'Add',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'delete.confirm' => 'Are you sure you want to Delete ?',
    'name' => 'Name',
    'email' => 'Email',
    'password' => 'Password',
    'authorities' => 'Authorities',
    'confirm' => 'Confirm',
    'cancel' => 'Cancel',
    'id' => 'ID',
    'action' => 'Action',
    'users' => 'Users',
    'add.user' => 'Add Admin',
    'phone' => 'Phone',
    'country' => 'Country',
    'choose.country' => 'Choose Country',
    'city' => 'City',
    'choose.country.first' => 'Choose Country First',
    'choose.city' => 'Choose City',
    'address' => 'Address',
    'gender' => 'Gender',
    'choose.gender' => 'Choose Gender',
    'dob' => 'Date Of Birth',
    'male' => 'Male',
    'female' => 'Female',
    'photo' => 'Photo',
    'sliders' => 'Sliders',
    'settings' => 'Settings',
    'site.details' => 'Site Details',
    'site.social' => 'Social media',
    'site.about' => 'About Site',
    'site.team' => 'Team Work',
    'site.gallery' => 'Gallery',
    'site.services' => 'Services',
    'site.time' => 'Time',
    'site.name' => 'Website Name',
    'site.logo' => 'Website Logo',
    'site.icon' => 'Website Icon',
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'instagram' => 'Instagram',
    'youtube' => 'Youtube',
    'snapchat' => 'Snapchat',
    'whatsapp' => 'Whatsapp',
    'google.plus' => 'Google Plus',
    'linkedin' => 'Linkedin',
    'pinterest' => 'Pinterest',
    'main.photo' => 'Main Photo',
    'job.title' => 'Job Title',
    'text' => 'Text',
    'show' => 'Text',
    'title' => 'Title',
    'day' => 'Day',
    'time' => 'Time',
    'status' => 'Status',
    'active' => 'Active',
    'pending' => 'Pending',
    'from' => 'From : ',
    'to' => 'To : ',
    'am' => 'AM',
    'pm' => 'PM',
    'messages' => 'Messages',
    'delete.all' => 'Delete All Message',
    'content' => 'Content',
    'shipping' => 'Content',
    'profile' => 'My Profile',
    'change.password' => 'Change Password',
    'old.password' => 'Old Password',
    'new.password' => 'New Password',
    'confirm.password' => 'Confirm New Password',
    'name.required' => 'Name Field Is Required',
    'name.unique' => 'Name Already Taken',
    'email.required' => 'Email Field Is Required',
    'email.unique' => 'Email Already Taken',
    'password.required' => 'Password Field Is Required',
    'password.min' => 'Minimum Password Is 6 Character Length',
    'roles.required' => 'Please Choose One Role Minimum',
    'add.success' => 'Added Successfully',
    'update.success' => 'Updated Successfully',
    'delete.success' => 'Deleted Successfully',
    'error' => 'Error Occurred , Try Again Later',
    'credentials.check' => 'Invalid Credentials',
    'shipping.required' => 'Shipping Field Is Required',
    'old_password.required' => 'Old Password Field Is Required',
    'new_password.required' => 'New Password Field Is Required',
    'new_password.min' => ' Minimum Password Is 6 Character Length',
    'new_password.max' => ' Maximum Password Is 15 Character Length',
    'new_password_confirmation.required' => 'New Password Confirmation Field Is Required',
    'new_password_confirmation.same' => 'Password And Confirmation Doesnt Match',
    'invalid.old_password' => 'Invalid Old Password',
    'site_name.required' => 'Site Name Field Is Required',
    'address.required' => 'Address Field Is Required',
    'phone.required' => 'Phone Field Is Required',
    'title.required' => 'Title Field Is Required',
    'details.required' => 'Details Field Is Required',
    'job_title.required' => 'Job Title Field Is Required',
    'img.required' => 'Photo Is Required ',
    'text.required' => 'Text Field Is Required',
    'phone.unique' => 'Phone Number Already Taken',










];