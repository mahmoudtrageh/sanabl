<?php

/* Admin Routes */

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin.'], function () {

    /*
     *  admin login and logout routes
     */

    Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {


        Route::get('/login', ['uses' => 'AuthController@index', 'as' => 'get.login']);

        Route::post('/login', ['uses' => 'AuthController@login', 'as' => 'post.login']);

        Route::get('/logout', ['uses' => 'AuthController@logout', 'as' => 'logout']);


    });


    /*Must Login Routes*/

    Route::group(['middleware' => 'admin'], function () {

      /*
       * Change Lang
       */
        Route::group( ['as' => 'lang.'], function () {
            Route::get('/lang', ['uses' => 'IndexController@changeLang', 'as' => 'change']);
        });

        /*
        * Dashboard
        */
        Route::group( ['as' => 'index.'], function () {
            Route::get('/', ['uses' => 'IndexController@index', 'as' => 'index']);
        });

        /*
         * Profile
         */
        Route::group(['prefix' => 'profile', 'as' => 'profile.'], function () {
            Route::get('/', ['uses' => 'ProfileController@index', 'as' => 'index']);
            Route::post('/update', ['uses' => 'ProfileController@update', 'as' => 'update']);
            Route::post('/update-password', ['uses' => 'ProfileController@updatePassword', 'as' => 'update.password']);
        });


        /*
         * Admins
         */
        Route::group(['prefix' => 'admins', 'as' => 'admins.'], function () {

            Route::get('/', ['uses' => 'AdminsController@index', 'as' => 'index']);
            Route::post('/add-admin', ['uses' => 'AdminsController@add', 'as' => 'add']);
            Route::post('/edit-admin', ['uses' => 'AdminsController@edit', 'as' => 'edit']);
            Route::post('/delete-admin', ['uses' => 'AdminsController@delete', 'as' => 'delete']);

        });

        /*
         * Roles
         */
        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function () {

            Route::get('/', ['uses' => 'RolesController@index', 'as' => 'index']);
            Route::post('/add-role', ['uses' => 'RolesController@add', 'as' => 'add']);
            Route::post('/edit-role', ['uses' => 'RolesController@edit', 'as' => 'edit']);
            Route::post('/delete-role', ['uses' => 'RolesController@delete', 'as' => 'delete']);

        });

        /*
         * Users
         */
        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {

            Route::get('/', ['uses' => 'UsersController@index', 'as' => 'index']);
            Route::post('/add-user', ['uses' => 'UsersController@add', 'as' => 'add']);
            Route::post('/edit-user', ['uses' => 'UsersController@edit', 'as' => 'edit']);
            Route::post('/delete-user', ['uses' => 'UsersController@delete', 'as' => 'delete']);
            Route::get('/cities', ['uses' => 'UsersController@getCities', 'as' => 'get.cities']);

        });

        /*
         * Sliders
         */
        Route::group(['prefix' => 'sliders', 'as' => 'sliders.'], function () {

            Route::get('/', ['uses' => 'SlidersController@index', 'as' => 'index']);
            Route::post('/add-slider', ['uses' => 'SlidersController@add', 'as' => 'add']);
            Route::post('/edit-slider', ['uses' => 'SlidersController@edit', 'as' => 'edit']);
            Route::post('/delete-slider', ['uses' => 'SlidersController@delete', 'as' => 'delete']);

        });


        /*
        * Settings
        */
        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {

            Route::get('/', ['uses' => 'SettingsController@index', 'as' => 'index']);

            // Site details
            Route::post('/edit-details', ['uses' => 'SettingsController@editSiteDetails', 'as' => 'edit.details']);

            // Social
            Route::post('/edit-social', ['uses' => 'SettingsController@editSocial', 'as' => 'edit.social']);

            // About us
            Route::post('/edit-about', ['uses' => 'SettingsController@editAbout', 'as' => 'edit.about']);

            // Team
            Route::post('/add-team', ['uses' => 'SettingsController@addTeam', 'as' => 'add.team']);
            Route::post('/edit-team', ['uses' => 'SettingsController@editTeam', 'as' => 'edit.team']);
            Route::post('/delete-team', ['uses' => 'SettingsController@deleteTeam', 'as' => 'delete.team']);

            // gallery department
            Route::post('/add-gallery-department', ['uses' => 'SettingsController@addGalleryDepartment', 'as' => 'add.gallery.department']);
            Route::post('/edit-gallery-department', ['uses' => 'SettingsController@editGalleryDepartment', 'as' => 'edit.gallery.department']);
            Route::post('/delete-gallery-department', ['uses' => 'SettingsController@deleteGalleryDepartment', 'as' => 'delete.gallery.department']);


            // Gallery
            Route::post('/add-gallery', ['uses' => 'SettingsController@addGallrey', 'as' => 'add.gallery']);
            Route::post('/edit-gallery', ['uses' => 'SettingsController@editGallrey', 'as' => 'edit.gallery']);
            Route::post('/delete-gallery', ['uses' => 'SettingsController@deleteGallrey', 'as' => 'delete.gallery']);

            Route::post('/add-article', ['uses' => 'SettingsController@addArticle', 'as' => 'add.article']);
            Route::post('/edit-article', ['uses' => 'SettingsController@editArticle', 'as' => 'edit.article']);
            Route::post('/delete-article', ['uses' => 'SettingsController@deleteArticle', 'as' => 'delete.article']);

            // Services
            Route::post('/add-service', ['uses' => 'SettingsController@addService', 'as' => 'add.service']);
            Route::post('/edit-service', ['uses' => 'SettingsController@editService', 'as' => 'edit.service']);
            Route::post('/delete-service', ['uses' => 'SettingsController@deleteService', 'as' => 'delete.service']);

            // Web Colors
            Route::post('/add-web-color', ['uses' => 'SettingsController@addWebColor', 'as' => 'add.web.color']);
            Route::post('/edit-web-color', ['uses' => 'SettingsController@editWebColor', 'as' => 'edit.web.color']);
            Route::post('/delete-web-color', ['uses' => 'SettingsController@deleteWebColor', 'as' => 'delete.web.color']);

            // Mobile Colors
            Route::post('/add-mobile-color', ['uses' => 'SettingsController@addMobileColor', 'as' => 'add.mobile.color']);
            Route::post('/edit-mobile-color', ['uses' => 'SettingsController@editMobileColor', 'as' => 'edit.mobile.color']);
            Route::post('/delete-mobile-color', ['uses' => 'SettingsController@deleteMobileColor', 'as' => 'delete.mobile.color']);

            // Web Slider
            Route::post('/add-web-slider', ['uses' => 'SettingsController@addWebSlider', 'as' => 'add.web.slider']);
            Route::post('/edit-web-slider', ['uses' => 'SettingsController@editWebSlider', 'as' => 'edit.web.slider']);
            Route::post('/delete-web-slider', ['uses' => 'SettingsController@deleteWebSlider', 'as' => 'delete.web.slider']);
            Route::post('/web-slider-status', ['uses' => 'SettingsController@webSliderStatus', 'as' => 'web.slider.status']);

            // Mobile Slider
            Route::post('/add-mobile-slider', ['uses' => 'SettingsController@addMobileSlider', 'as' => 'add.mobile.slider']);
            Route::post('/edit-mobile-slider', ['uses' => 'SettingsController@editMobileSlider', 'as' => 'edit.mobile.slider']);
            Route::post('/delete-mobile-slider', ['uses' => 'SettingsController@deleteMobileSlider', 'as' => 'delete.mobile.slider']);
            Route::post('/mobile-slider-status', ['uses' => 'SettingsController@mobileSliderStatus', 'as' => 'mobile.slider.status']);


            // Time

            Route::post('/edit-time', ['uses' => 'SettingsController@editTime', 'as' => 'edit.time']);
            Route::post('/edit-status', ['uses' => 'SettingsController@editStatus', 'as' => 'edit.status']);

            Route::group(['prefix' => 'countries'], function () {

                Route::post('/add-country', ['uses' => 'SettingsController@AddCountry', 'as' => 'add.country']);
                Route::post('/edit-country', ['uses' => 'SettingsController@EditCountry', 'as' => 'edit.country']);
                Route::post('/delete-country', ['uses' => 'SettingsController@DeleteCountry', 'as' => 'delete.country']);

            });

            Route::group(['prefix' => 'provinces'], function () {

                Route::get('/{id}', ['uses' => 'SettingsController@Provinces', 'as' => 'provinces']);
                Route::post('/add-province', ['uses' => 'SettingsController@AddProvince', 'as' => 'add.province']);
                Route::post('/edit-province', ['uses' => 'SettingsController@EditProvince', 'as' => 'edit.province']);
                Route::post('/delete-province', ['uses' => 'SettingsController@DeleteProvince', 'as' => 'delete.province']);

            });

            Route::group(['prefix' => 'cities'], function () {

                Route::get('/{id}', ['uses' => 'SettingsController@Cities', 'as' => 'cities']);
                Route::post('/add-city', ['uses' => 'SettingsController@AddCity', 'as' => 'add.city']);
                Route::post('/edit-city', ['uses' => 'SettingsController@EditCity', 'as' => 'edit.city']);
                Route::post('/delete-city', ['uses' => 'SettingsController@DeleteCity', 'as' => 'delete.city']);

            });

            Route::group(['prefix' => 'areas'], function () {

                Route::get('/{id}', ['uses' => 'SettingsController@Areas', 'as' => 'areas']);
                Route::post('/add-area', ['uses' => 'SettingsController@AddArea', 'as' => 'add.area']);
                Route::post('/edit-area', ['uses' => 'SettingsController@EditArea', 'as' => 'edit.area']);
                Route::post('/delete-area', ['uses' => 'SettingsController@DeleteArea', 'as' => 'delete.area']);
            });
        });

        /*
         * Messages
         */
        Route::group(['prefix' => 'messages', 'as' => 'messages.'], function () {
            Route::get('/', ['uses' => 'MessagesController@index', 'as' => 'index']);
            Route::post('/delete-message', ['uses' => 'MessagesController@delete', 'as' => 'delete']);
            Route::post('/delete-all', ['uses' => 'MessagesController@deleteAll', 'as' => 'delete.all']);

        });

        /*
         * Countries
         */
        Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
            Route::get('/', ['uses' => 'CountriesController@index', 'as' => 'index']);
            Route::post('/add-country', ['uses' => 'CountriesController@add', 'as' => 'add.country']);
            Route::post('/edit-country', ['uses' => 'CountriesController@edit', 'as' => 'edit.country']);
            Route::post('/delete-country', ['uses' => 'CountriesController@delete', 'as' => 'delete.country']);

        });

        /*
         * Cities
         */
        Route::group(['prefix' => 'cities', 'as' => 'cities.'], function () {
            Route::get('/', ['uses' => 'CitiesController@index', 'as' => 'index']);
            Route::post('/add-city', ['uses' => 'CitiesController@add', 'as' => 'add.city']);
            Route::post('/edit-city', ['uses' => 'CitiesController@edit', 'as' => 'edit.city']);
            Route::post('/delete-city', ['uses' => 'CitiesController@delete', 'as' => 'delete.city']);

        });

    });

});

Route::group(['namespace' => 'Site'], function () {

    /*
     * Index
     */

    Route::get('', 'IndexController@index')->name('site-index');

});