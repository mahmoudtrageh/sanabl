<?php

use Faker\Generator as Faker;

$factory->define(App\SiteAbout::class, function (Faker $faker) {
    return [
        'img1'=>'123',
        'title'=>$faker->title,
        'details'=>$faker->paragraph(3),
    ];
});
