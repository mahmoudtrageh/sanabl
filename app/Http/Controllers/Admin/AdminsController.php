<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminsController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:1');
    }
    public function index()
    {
        $admins = Admin::where('isSuper',0)->where('id', '!=', auth()->guard('admin')->user()->id)->get();
        $roles = Role::all();
        return view('admin.pages.admins.admins', compact('admins', 'roles'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:admins,name|max:50',
            'email' => 'required|unique:admins,email|max:50',
            'password' => 'required|min:6|max:50',
            'roles' => 'required',
        ],
            [
                'name.required' =>trans('admin.name.required'),
                'name.unique' => trans('admin.name.unique'),
                'email.required' => trans('admin.email.required'),
                'email.unique' =>trans('admin.email.unique'),
                'password.required' => trans('admin.password.required'),
                'password.min' => trans('admin.password.min'),
                'roles.required' => trans('admin.roles.required'),
            ]);
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        $admin = Admin::create($input);
        if ($admin) {
            $admin->roles()->attach($request->roles);
            return redirect()->back()->with('success', trans('admin.add.success'));
        }
        return redirect()->back()->with('error', trans('admin.error'));
    }

    public function edit(Request $request)
    {
        $checker = Admin::find($request->admin_id);
        $this->validate($request, [
            'name' => 'required|max:50|unique:admins,name,' . $checker->id,
            'email' => 'required|max:50|unique:admins,email,' . $checker->id,
            'password' => 'nullable|min:6|max:50',
            'roles' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'name.unique' => trans('admin.name.unique'),
                'email.required' =>trans('admin.email.required'),
                'email.unique' => trans('admin.email.unique'),
                'password.min' => trans('admin.password.min'),
                'roles.required' =>trans('admin.roles.required'),
            ]);
        $input = $request->all();
        if ($request->password) {
            $input['password'] = bcrypt($request->password);
        }
        $input['password'] = $checker->password;
        $admin = $checker->update($input);
        if ($admin) {
            $checker->roles()->sync($request->roles);
            return redirect()->back()->with('success', trans('admin.update.success'));
        }
        return redirect()->back()->with('error', trans('admin.error'));
    }

    public function delete(Request $request)
    {
        $checker = Admin::find($request->admin_id);
        $checker->roles()->detach();
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }
}
