<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:2');
    }

    public function index()
    {
        $users = User::all();
        $countries = Country::all();
        return view('admin.pages.users.users', compact('users', 'countries'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:users,name|max:50',
            'email' => 'required|unique:users,email|max:50',
            'password' => 'required|min:6|max:50',
            'phone' => 'required|unique:users,phone|numeric',

        ],
            [
                'name.required' =>trans('admin.name.required'),
                'name.unique' => trans('admin.name.unique'),
                'email.required' => trans('admin.email.required'),
                'email.unique' =>trans('admin.email.unique'),
                'password.required' => trans('admin.password.required'),
                'password.min' => trans('admin.password.min'),
                'phone.unique' => trans('admin.phone.unique'),

            ]);
        $input = $request->all();
        $input['password'] = bcrypt($request->password);
        User::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));

    }

    public function edit(Request $request)
    {
        $checker = User::find($request->user_id);
        $this->validate($request, [
            'name' => 'required|max:50|unique:users,name,' . $checker->id,
            'email' => 'required|max:50|unique:users,email,' . $checker->id,
            'password' => 'nullable|min:6|max:50',
            'phone' => 'required|numeric|unique:users,phone,' . $checker->id,

        ],
            [
                'name.required' =>trans('admin.name.required'),
                'name.unique' => trans('admin.name.unique'),
                'email.required' => trans('admin.email.required'),
                'email.unique' =>trans('admin.email.unique'),
                'password.required' => trans('admin.password.required'),
                'password.min' => trans('admin.password.min'),
                'phone.unique' => trans('admin.phone.unique'),

            ]);
        $input = $request->all();
        if ($request->password) {
            $input['password'] = bcrypt($request->password);
        }
        $input['password'] = $checker->password;
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function delete(Request $request)
    {
        $checker = User::find($request->user_id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    public function getCities(Request $request){
        $cities = Country::find($request->id)->cities ;
        return ['status'=>'1' , 'cities' => $cities];
    }
}
