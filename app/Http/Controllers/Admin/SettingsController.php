<?php

namespace App\Http\Controllers\Admin;

use App\Activity;
use App\Area;
use App\Article;
use App\City;
use App\Country;
use App\Gallery;
use App\GalleryDepartment;
use App\MobileColor;
use App\MobileSlider;
use App\Province;
use App\Seminar;
use App\Service;
use App\SiteAbout;
use App\SiteDetail;
use App\Social;
use App\Sponser;
use App\Team;
use App\Video;
use App\WebColor;
use App\WebSlider;
use App\WorkDay;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:4');
    }

    public function index()
    {

        $site_details = SiteDetail::first();
        $social = Social::first();
        $about = SiteAbout::first();
        $teams = Team::all();
        $gallery_departments = GalleryDepartment::all();
        $galleries = Gallery::all();
        $articles = Article::all();
        $services = Service::all();
        $work_days = WorkDay::all();
        $countries = Country::all();
        $web_colors = WebColor::all();
        $mobile_colors = MobileColor::all();
        $web_sliders = WebSlider::all();
        $mobile_sliders = MobileSlider::all();

        return view('admin.pages.settings.settings', compact('site_details',
            'social', 'about', 'teams', 'galleries', 'services', 'work_days',
            'gallery_departments', 'articles', 'countries', 'provinces',
            'cities', 'areas', 'mobile_colors', 'web_colors', 'web_sliders', 'mobile_sliders'));
    }


    //site details
    public function editSiteDetails(Request $request)
    {
        $checker = SiteDetail::first();
        $input = $request->all();
        if ($request->file('icon')) {
            $destination = public_path('uploads/site_details');
            $input['icon'] = update_file($request->file('icon'), $checker, 'icon', $destination);
        }
        if ($request->file('logo')) {
            $destination = public_path('uploads/site_details');
            $input['logo'] = update_file($request->file('logo'), $checker, 'logo', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    //site social
    public function editSocial(Request $request)
    {
        Social::first()->update($request->all());
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    // site about
    public function editAbout(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'details' => 'required|max:1000',
            'title_en' => 'required|max:1000',
            'details_en' => 'required|max:1000',

        ],
            [
                'title.required' => trans('admin.title.required'),
                'details.required' => trans('admin.details.required'),
                'title_en.required' => 'برجاء إدخال العنوان الإنجليزي',
                'details_en.required' => 'برحاء إدخال التفاصيل الانجليزي',

            ]);

        $checker = SiteAbout::first();
        $input = $request->all();
        if ($request->file('img1')) {
            $destination = public_path('uploads/site_about');
            $input['img1'] = update_file($request->file('img1'), $checker, 'img1', $destination);
        }
        if ($request->file('img2')) {
            $destination = public_path('uploads/site_about');
            $input['img2'] = update_file($request->file('img2'), $checker, 'img2', $destination);
        }
        if ($request->file('img3')) {
            $destination = public_path('uploads/site_about');
            $input['img3'] = update_file($request->file('img3'), $checker, 'img3', $destination);
        }
        if ($request->file('img4')) {
            $destination = public_path('uploads/site_about');
            $input['img4'] = update_file($request->file('img4'), $checker, 'img4', $destination);
        }
        if ($request->file('img5')) {
            $destination = public_path('uploads/site_about');
            $input['img5'] = update_file($request->file('img5'), $checker, 'img5', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    // Team
    public function addTeam(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'name' => 'required|max:100',
            'name_en' => 'required|max:100',
            'title_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'title.required' =>trans('admin.job_title.required'),
                'name_en.required' =>'',
                'title_en.required' => '',
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/team');
        $input['img'] = add_file($request->file('img'), $destination);
        Team::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editTeam(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:100',
            'name' => 'required|max:1000',
            'title_en' => 'required|max:100',
            'name_en' => 'required|max:1000',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'title.required' =>trans('admin.job_title.required'),
                'name_en.required' =>'',
                'title_en.required' =>'',
            ]);
        $checker = Team::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/team');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteTeam(Request $request)
    {
        $checker = Team::find($request->id);
        $destination = public_path('uploads/team');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Gallery Department
    public function addGalleryDepartment(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>'',
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/gallery');
        $input['img'] = add_file($request->file('img'), $destination);
        GalleryDepartment::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editGalleryDepartment(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:1000',
            'name_en' => 'required|max:1000',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' =>'',
            ]);
        $checker = GalleryDepartment::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/gallery');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteGalleryDepartment(Request $request)
    {
        $checker = GalleryDepartment::find($request->id);
        $destination = public_path('uploads/gallery');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Gallery
    public function addGallrey(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'name_en' => 'required|max:100',
            'department_ar' => 'required|max:100',
            'department_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'img.required' => trans('admin.phone.required'),
                'department_ar.required' => trans('admin.phone.required'),
                'department_en.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/gallery');
        $input['img'] = add_file($request->file('img'), $destination);
        Gallery::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editGallrey(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'department_ar' => 'required|max:1000',
            'department_en' => 'required|max:1000',

        ],
            [
                'name.required' => trans('admin.name.required'),
                'department_ar.required' => trans('admin.text.required'),
                'department_en.required' => trans('admin.text.required'),
            ]);
        $checker = Gallery::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/gallery');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteGallrey(Request $request)
    {
        $checker = Gallery::find($request->id);
        $destination = public_path('uploads/gallery');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Gallery
    public function addArticle(Request $request)
    {
        $this->validate($request, [
            'author_ar' => 'required|max:100',
            'author_en' => 'required|max:100',
            'date' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'content_ar' => 'required',
            'content_en' => 'required',
            'img' => 'required',

        ],
            [
                'author_ar.required' => trans('admin.name.required'),
                'author_en.required' => trans('admin.phone.required'),
                'date.required' => trans('admin.phone.required'),
                'title_ar.required' => trans('admin.phone.required'),
                'title_en.required' => trans('admin.phone.required'),
                'content_ar.required' => trans('admin.phone.required'),
                'content_en.required' => trans('admin.phone.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/article');
        $input['img'] = add_file($request->file('img'), $destination);
        Article::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editArticle(Request $request)
    {
        $this->validate($request, [
            'author_ar' => 'required|max:100',
            'author_en' => 'required|max:100',
            'date' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'content_ar' => 'required',
            'content_en' => 'required',
            'img' => 'required',

        ],
            [
                'author_ar.required' => trans('admin.name.required'),
                'author_en.required' => trans('admin.phone.required'),
                'date.required' => trans('admin.phone.required'),
                'title_ar.required' => trans('admin.phone.required'),
                'title_en.required' => trans('admin.phone.required'),
                'content_ar.required' => trans('admin.phone.required'),
                'content_en.required' => trans('admin.phone.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $checker = Article::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/article');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteArticle(Request $request)
    {
        $checker = Article::find($request->id);
        $destination = public_path('uploads/article');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    // Services
    public function addService(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
            'img' => 'required',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name.required'),
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/services');
        $input['img'] = add_file($request->file('img'), $destination);
        Service::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editService(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required|max:100',
            'name_en' => 'required|max:100',
        ],
            [
                'name_ar.required' => trans('admin.name.required'),
                'name_en.required' => trans('admin.name.required'),
            ]);
        $checker = Service::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/services');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteService(Request $request)
    {
        $checker = Service::find($request->id);
        $destination = public_path('uploads/services');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // web Color
    public function addWebColor(Request $request)
    {
        $input = $request->all();
        WebColor::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editWebColor(Request $request)
    {
        $checker = WebColor::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteWebColor(Request $request)
    {
        $checker = WebColor::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Services
    public function addMobileColor(Request $request)
    {
        $input = $request->all();
        MobileColor::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editMobileColor(Request $request)
    {
        $checker = MobileColor::find($request->id);
        $input = $request->all();
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteMobileColor(Request $request)
    {
        $checker = MobileColor::find($request->id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }

    // Services
    public function addWebSlider(Request $request)
    {
        $this->validate($request, [
            'img' => 'required',
        ],
            [
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/sliders');
        $input['img'] = add_file($request->file('img'), $destination);
        WebSlider::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editWebSlider(Request $request)
    {
        $checker = WebSlider::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/services');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteWebSlider(Request $request)
    {
        $checker = WebSlider::find($request->id);
        $destination = public_path('uploads/sliders');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    // Services
    public function addMobileSlider(Request $request)
    {
        $this->validate($request, [
            'img' => 'required',
        ],
            [
                'img.required' => trans('admin.phone.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/sliders');
        $input['img'] = add_file($request->file('img'), $destination);
        MobileSlider::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function editMobileSlider(Request $request)
    {
        $checker = MobileSlider::find($request->id);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/sliders');
            $input['img'] = update_file($request->file('img'), $checker, 'img', $destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function deleteMobileSlider(Request $request)
    {
        $checker = MobileSlider::find($request->id);
        $destination = public_path('uploads/sliders');
        delete_file($checker, 'img', $destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }


    // Working Days

    public function editTime(Request $request)
    {
        WorkDay::find($request->id)->update($request->all());
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function editStatus(Request $request)
    {
        $day = WorkDay::find($request->id);
        $day->is_active = $request->status;
        $day->save();
        return response()->json(["status" => "1", 'message' => 'Success']);
    }


    public function AddCountry(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (Country::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditCountry(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Country::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteCountry(Request $request)
    {
        Country::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }

    public function Provinces($id)
    {

        $provinces = Country::find($id)->provinces;

        return view('admin.pages.settings.provinces', compact(  'provinces', 'id'));

    }

    public function AddProvince(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'name_en' => 'required',
        ],
            [
                'name.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (Province::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditProvince(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'name_en' => 'required',
        ],
            [
                'name.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Province::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteProvince(Request $request)
    {
        Province::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }

    public function Cities($id)
    {

        $cities = Province::find($id)->cities;
        return view('admin.pages.settings.cities', compact('cities', 'id'));

    }

    public function AddCity(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (City::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditCity(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = City::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteCity(Request $request)
    {
        City::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }

    public function Areas($id)
    {

        $areas = City::find($id)->areas;
        return view('admin.pages.settings.areas', compact('areas', 'id'));

    }

    public function AddArea(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);

        if (Area::create($request->all())) {
            return redirect()->back()->withSuccess('تم الاضافة');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function EditArea(Request $request)
    {
        $this->validate($request, [
            'name_ar' => 'required',
            'name_en' => 'required',
        ],
            [
                'name_ar.required' => 'برجاء إدخال الاسم العربي',
                'name_en.required' => 'برجاء ادخال الاسم الانجليزي',
            ]);
        $checker = Area::find($request->id);
        if ($checker->update($request->all())) {
            return redirect()->back()->withSuccess('تم التعديل');
        }
        return redirect()->back()->withInput($request->all());

    }

    public function DeleteArea(Request $request)
    {
        Area::find($request->id)->delete();
        return redirect()->back()->with('deleted', 'تم الحذف');

    }

}
