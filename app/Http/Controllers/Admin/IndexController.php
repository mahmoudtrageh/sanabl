<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        return view('admin.pages.dashboard.index');
    }

    public function changeLang(Request $request){
        $lang = $request->lang;
        $user = auth()->guard('admin')->user();
        $user->update([
            'lang'=>$lang
            ]);
        return redirect()->back();
    }
}
