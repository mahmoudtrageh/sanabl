<?php

namespace App\Http\Controllers\Admin;

use App\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:3');
    }
    public function index()
    {
        $sliders = Slider::all();
        return view('admin.pages.sliders.sliders', compact('sliders'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'img' => 'required',
            'name' => 'required',
        ],
            [
                'img.required' => trans('admin.img.required'),
                'name.required' => trans('admin.name.required'),
            ]);
        $input = $request->all();
        $destination = public_path('uploads/slider');
        $input['img'] = add_file($request->file('img'),$destination);
        Slider::create($input);
        return redirect()->back()->with('success', trans('admin.add.success'));
    }

    public function edit(Request $request)
    {
        $checker = Slider::find($request->slider_id);
        $this->validate($request, [
            'name' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
            ]);
        $input = $request->all();
        if ($request->file('img')) {
            $destination = public_path('uploads/slider');
            $input['img'] = update_file($request->file('img'),$checker,'img',$destination);
        }
        $checker->update($input);
        return redirect()->back()->with('success', trans('admin.update.success'));
    }

    public function delete(Request $request)
    {
        $checker = Slider::find($request->slider_id);
        $destination = public_path('uploads/slider');
        delete_file($checker,'img',$destination);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }
}
