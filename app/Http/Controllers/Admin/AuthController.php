<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public  function index()
    {
        return view('admin.pages.auth.auth');
    }
    public function login(Request $request){
      $v = validator($request->all(), [
            'email'=>'required',
            'password'=>'required',
        ] , [
            'email.required'=>trans('admin.email.required'),
            'password.required'=>trans('admin.password.required'),
        ]);
      if ($v->fails()){
          return response()->json(['status'=>'0','type'=>'validation_error','message'=>$v->errors()->first()]);
      }
        if (\Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password]))

        {
            return response()->json(['status'=>'1','type'=>'login_success','url'=>route('admin.index.index')]);
        }
        elseif (\Auth::guard('admin')->attempt(['name'=>$request->email,'password'=>$request->password]))
        {
            return response()->json(['status'=>'1','type'=>'login_success','url'=>route('admin.index.index')]);
        }
        return response()->json(['status'=>'0','type'=>'login_error','message'=>trans('admin.credentials.check')]);
    }
    public function logout(){

        auth()->guard('admin')->logout();
        return redirect()->route('admin.auth.get.login');

    }
}
