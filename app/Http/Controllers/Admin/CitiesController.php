<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:6');
    }
    public function index(Request $request)
    {
        $cities = Country::find($request->id)->cities;
        $id = $request->id ;
        return view('admin.pages.countries&cities.cities', compact('cities','id'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'shipping' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'shipping.required' => trans('admin.shipping.required'),
            ]);
        $input = $request->all();
        $city = Country::find($request->country_id)->cities()->create($input);
        if ($city) {
            return redirect()->back()->with('success', trans('admin.add.success'));
        }
        return redirect()->back()->with('error', trans('admin.error'));
    }

    public function edit(Request $request)
    {
        $checker = City::find($request->city_id);
        $this->validate($request, [
            'name' => 'required',
            'shipping' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),
                'shipping.required' => trans('admin.shipping.required'),
                ]);
        $input = $request->all();
        $city = $checker->update($input);
        if ($city) {
            return redirect()->back()->with('success', trans('admin.update.success'));
        }
        return redirect()->back()->with('error', trans('admin.error'));
    }

    public function delete(Request $request)
    {
        $checker = City::find($request->city_id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }
}
