<?php

namespace App\Http\Controllers\Admin;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('adminPermissions:6');
    }
    public function index()
    {
        $countries = Country::all();
        return view('admin.pages.countries&cities.countries', compact('countries'));
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ],
            [
                'name.required' => trans('admin.name.required'),

            ]);
        $input = $request->all();
        $country = Country::create($input);
        if ($country) {
            return redirect()->back()->with('success', trans('admin.add.success'));
        }
            return redirect()->back()->with('error', trans('admin.error'));
    }

    public function edit(Request $request)
    {
        $checker = Country::find($request->country_id);
        $this->validate($request, [
            'name' => 'required',

        ],
            [
                'name.required' => trans('admin.name.required'),
            ]);
        $input = $request->all();
        $country = $checker->update($input);
        if ($country) {
            return redirect()->back()->with('success',trans('admin.update.success'));
        }
        return redirect()->back()->with('error', trans('admin.error'));
    }

    public function delete(Request $request)
    {
        $checker = Country::find($request->country_id);
        $checker->delete();
        return redirect()->back()->with('success', trans('admin.delete.success'));
    }
}
