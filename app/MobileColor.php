<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileColor extends Model
{
    protected  $fillable=[
        'first',
        'second',
        'third',
        'fourth',
    ];
}
