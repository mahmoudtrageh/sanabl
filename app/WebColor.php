<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebColor extends Model
{
    protected  $fillable=[
        'first',
        'second',
        'third',
        'fourth',
    ];
}
