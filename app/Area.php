<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $fillable = [

        'name_ar',
        'name_en',
        'city_id',
        'shipping',
    ];
}
