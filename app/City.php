<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [

        'name_ar',
        'name_en',
        'province_id'

    ];

    protected $table = 'cities';

    public function areas(){
        return $this->hasMany(Area::class);
    }
}
