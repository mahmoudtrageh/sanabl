<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteAbout extends Model
{
     protected  $fillable=[

             'img1',
             'img2',
             'img3',
             'img4',
             'img5',
             'title',
         'title_en',
             'details',
         'details_en',

         ];
}
