<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebSlider extends Model
{
    protected  $fillable=[
        'name_ar',
        'name_en',
        'img',
        'is_active',
    ];
}
