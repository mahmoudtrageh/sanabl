<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
     protected  $fillable=[

             'facebook',
             'twitter',
             'instagram',
             'youtube',
             'snapchat',
             'whatsapp',
             'google_plus',
             'linked',
             'pinterest',

         ];
}
