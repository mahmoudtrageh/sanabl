<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected  $fillable=[
        'name_ar',
        'name_en',
        'text_ar',
        'text_en',
        'img',
    ];
}
