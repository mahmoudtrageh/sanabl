const mix = require('laravel-mix');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//    .sass('resources/sass/app.scss', 'public/css');

// admin
mix.styles([
    'public/backEnd/assets/vendors/vendors.bundle.css',
    'public/backEnd/assets/app/app.bundle.css',
    'public/backEnd/assets/pages/datatables.css',
    'public/backEnd/css/my-style.css',
],'public/css/admin.css');



mix.scripts([
    'public/backEnd/assets/vendors/vendors.bundle.js',
    'public/backEnd/assets/app/app.bundle.js',
],'public/js/admin.js');


mix.styles([
    'public/site/css/font-awesome.css',
    'public/site/css/bootstrap.css',
    'public/site/css/slick.css',
    'public/site/css/jquery.fancybox.css',
    'public/site/css/theme-color/default-theme.css',
    'public/site/style.css',
],'public/css/admin.css');



mix.scripts([
    'public/site/js/jquery.min.js',
    'public/site/js/bootstrap.js',
    'public/site/js/bootstrap.js',
    'public/site/js/waypoints.js',
    'public/site/js/waypoints.js',
    'public/site/js/bootstrap-datepicker.js',
    'public/site/js/jquery.mixitup.js',
    'public/site/js/jquery.fancybox.pack.js',
    'public/site/js/custom.js',
],'public/js/admin.js');

