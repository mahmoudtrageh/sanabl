<?php

namespace iki\FixedAdmin\Facades;

use Illuminate\Support\Facades\Facade;

class FixedAdmin extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'fixedadmin';
    }
}
