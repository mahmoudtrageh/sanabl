<?php

namespace iki\FixedAdmin;

use Illuminate\Support\ServiceProvider;

class FixedAdminServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
         $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'iki');
         $this->loadViewsFrom(__DIR__.'/../resources/views', 'iki');
         $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
         $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->publishes([
            __DIR__.'/iki/FixedAdmin/config/fixedAdmin.php' => config_path('fixedAdmin.php'),
            __DIR__.'/../resources/views' => resource_path('views/vendor/courier'),

        ]);
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/fixedadmin.php', 'fixedadmin');

        // Register the service the package provides.
        $this->app->singleton('fixedadmin', function ($app) {
            return new FixedAdmin;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['fixedadmin'];
    }
    
    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__.'/../config/fixedadmin.php' => config_path('fixedadmin.php'),
        ], 'fixedadmin.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/iki'),
        ], 'fixedadmin.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/iki'),
        ], 'fixedadmin.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/iki'),
        ], 'fixedadmin.views');*/

        // Registering package commands.
        // $this->commands([]);
    }
}
